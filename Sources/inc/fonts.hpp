/* fonts.hpp
 * Copyright Eric Bachard  2019 July 30th / Licence GPL v2
 * This file is under GPL v2 license
 * contains a list of macros allowing the user to select
 * another Microsoft Windows Font used in his application
 * (just in case the font is broken, what occurs regularly)
 */


#ifndef __FONTS_HPP_
#define __FONTS_HPP_

#ifdef NATIVE_BUILD

/* Unix fonts */

#define UNIX_TTF_FONT_UBUNTU_REGULAR         "./fonts/Ubuntu-R.ttf"
#define UNIX_TTF_FONT_UBUNTU_BOLD            "./fonts/Ubuntu-B.ttf"

// LINUX ONLY : install MS fonts with msttcorefonts is mandatory
#define UNIX_TTF_FONT_COMIC_SANS_MS          "/usr/share/fonts/truetype/msttcorefonts/comic.ttf"
#define UNIX_TTF_FONT_COMIC_SANS_MS_BOLD     "/usr/share/fonts/truetype/msttcorefonts/comicbd.ttf"
#define UNIX_TTF_FONT_ARIAL                  "/usr/share/fonts/truetype/msttcorefonts/arial.ttf"
#define UNIX_TTF_FONT_ARIAL_BLACK            "/usr/share/fonts/truetype/msttcorefonts/ariblk.ttf"
#define UNIX_TTF_FONT_VERDANA                "/usr/share/fonts/truetype/msttcorefonts/verdana.ttf"
#define UNIX_TTF_FONT_TIMES_NEW_ROMAN        "/usr/share/fonts/truetype/msttcorefonts/times.ttf"
#define UNIX_TTF_FONT_IMPACT                 "/usr/share/fonts/truetype/msttcorefonts/impact.ttf"

//#define TEST_SYSTEM_DROID_FONT
#ifdef TEST_SYSTEM_DROID_FONT
#define UNIX_TTF_FONT_DROID_SANS              "/usr/share/fonts/truetype/miniDart/DroidSans.ttf"
#else
#define UNIX_TTF_FONT_DROID_SANS              "./fonts/DroidSans.ttf"
#endif

#define SYSTEM_FONT_MAX      10
#define DEFAULT_APPLICATION_FONT UNIX_TTF_FONT_DROID_SANS

#else

// SOURCE : https://docs.microsoft.com/en-us/typography/fonts/windows_7_font_list

/* Windows system fonts */
#define WINDOWS_TTF_FONT_ARIAL               "C:\\Windows\\Fonts\\arial.ttf"
#define WINDOWS_TTF_FONT_ARIAL_ITALIC        "C:\\Windows\\Fonts\\ariali.ttf"
#define WINDOWS_TTF_FONT_ARIAL_BOLD          "C:\\Windows\\Fonts\\arialbd.ttf"
#define WINDOWS_TTF_FONT_ARIAL_BLACK         "C:\\Windows\\Fonts\\ariblk.ttf"
#define WINDOWS_TTF_FONT_CALIBRI             "C:\\Windows\\Fonts\\Calibri.ttf"
#define WINDOWS_TTF_FONT_COMIC_SANS_MS       "C:\\Windows\\Fonts\\comic.ttf"
#define WINDOWS_TTF_FONT_COMIC_SANS_MS_BOLD  "C:\\Windows\\Fonts\\comicbd.ttf"
#define WINDOWS_TTF_FONT_TAHOMA              "C:\\Windows\\Fonts\\tahoma.ttf"
#define WINDOWS_TTF_FONT_TAHOMA_BOLD         "C:\\Windows\\Fonts\\tahomabd.ttf"
#define WINDOWS_TTF_FONT_TIMES_NEW_ROMAN     "C:\\Windows\\Fonts\\times.ttf"
#define WINDOWS_TTF_FONT_VERDANA             "C:\\Windows\\Fonts\\verdana.ttf"
#define WINDOWS_TTF_FONT_VERDANA_BOLD        "C:\\Windows\\Fonts\\verdanab.ttf"
#define WINDOWS_TTF_FONT_VERDANA_ITALIC      "C:\\Windows\\Fonts\\verdanai.ttf"
#define WINDOWS_TTF_FONT_WEBDINGS            "C:\\Windows\\Fonts\\webdings.ttf"
#define WINDOWS_TTF_FONT_WINGDING            "C:\\Windows\\Fonts\\wingding.ttf"

#define SYSTEM_FONT_MAX  15
#define DEFAULT_APPLICATION_FONT WINDOWS_TTF_FONT_ARIAL

#endif  /* NATIVE_BUILD */
#endif /*  __FONTS_HPP_  */

