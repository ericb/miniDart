/*
 * Fichier d'en tête version.h pour le projet miniDart
 * Auteur : Eric Bachard  / lundi 6 mars 2022, 14:35:03 (UTC+0200)
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __VERSION_H
#define __VERSION_H

#define APP_NAME miniDart

#define VERSION_MAJOR   0
#define VERSION_MINOR   9
#define VERSION_PATCH   8

#endif  /*  miniDart version number */
