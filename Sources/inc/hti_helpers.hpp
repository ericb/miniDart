/*
  File hti_helpers.hpp, belongs to seekGui project
  Copyright : Eric Bachard 2020 december 19th 22:56:50
  This file is under GPL v2 license
 */

#ifndef __HTI_HELPERS_HPP
#define __HTI_HELPERS_HPP

#include "string.h"

enum ColorPalettes
{
    WHITE_HOT_COLOR_PALETTE       ,
    BLACK_HOT_COLOR_PALETTE       ,
    IRON_RAINBOW_COLOR_PALETTE    ,
    RAINBOW_HC_COLOR_PALETTE      ,
    RAINBOW_COLOR_PALETTE         ,
    IRON_GREY_COLOR_PALETTE       ,
    RED_HOT_COLOR_PALETTE         ,
    BLACK_TO_YELLOW_COLOR_PALETTE ,
    BLUE_TO_RED_COLOR_PALETTE     ,
    BLUE_TO_YELLOW_COLOR_PALETTE
} COLOR_PALETTES;

#define SHUTTER_RESET_        0x8000
#define COLOR_PALETTES_OFFSET  0x8800

#endif  /* __HTI_HELPERS_HPP */
