/*
 * File audiodevice.cpp belongs to miniDart project
 * Copyright : Eric Bachard  / 2019 July 7th, 14:35:03 (UTC+0200)
 * This document is under GPL v2 license
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <cstdint>
#include <cstring>
#include <vector>
#include <SDL2/SDL.h>

#ifndef _AUDIO_DEVICE_HPP
#define _AUDIO_DEVICE_HPP

#define DEFAULT_RECORDING_AUDIO_DEVICE 0

// maximum recording time (2s)
const int MAX_RECORDING_SECONDS = 2;

#define RECORDING_BUFFER_SECONDS (MAX_RECORDING_SECONDS + 1)


#define OPEN_ERROR        1
#define MALLOC_ERROR      2
#define ANY_ERROR         3
#define ACCESS_ERROR      4
#define FORMAT_ERROR      5
#define RATE_ERROR        6
#define CHANNELS_ERROR    7
#define PARAMS_ERROR      8
#define PREPARE_ERROR     9
#define FOPEN_ERROR       10
#define FCLOSE_ERROR      11
#define SNDREAD_ERROR     12
#define START_ERROR       13

/*  WAV HEADER (see https://docs.fileformat.com/audio/wav/)*/

// Positions	Sample Value	Description
// 1 - 4	“RIFF”	Marks the file as a riff file. Characters are each 1 byte long.
// 5 - 8	File size (integer)	Size of the overall file - 8 bytes, in bytes (32-bit integer). Typically, you’d fill this in after creation.
// 9 -12	"WAVE”	File Type Header. For our purposes, it always equals “WAVE”.
// 13-16	"fmt "	Format chunk marker. Includes trailing null
// 17-20	16	Length of format data as listed above
// 21-22	1	Type of format (1 is PCM) - 2 byte integer
// 23-24	2	Number of Channels - 2 byte integer
// 25-28	44100	Sample Rate - 32 byte integer. Common values are 44100 (CD), 48000 (DAT). Sample Rate = Number of Samples per second, or Hertz.
// 29-32	176400	(Sample Rate * BitsPerSample * Channels) / 8.
// 33-34	4	(BitsPerSample * Channels) / 8.1 - 8 bit mono2 - 8 bit stereo/16 bit mono4 - 16 bit stereo
// 35-36	16	Bits per sample
// 37-40	“data”	“data” chunk header. Marks the beginning of the data section.
// 41-44	File size (data)	Size of the data section.
// Sample values are given above for a 16-bit stereo source.


// this is the bitrate
//#define MAX_BUF_SIZE	512
#define MAX_BUF_SIZE	1024
//  8s
//#define MAX_SAMPLES	256000
#define MAX_SAMPLES	512000
//#define MAX_SAMPLES	1024000

typedef struct WavHeader // Wav file header structure
{
    uint8_t FileID[4];      // bytes 1, 2, 3, 4
    uint32_t FileSize;      // bytes 5, 6, 7, 8
    uint8_t Format[4];      // bytes 9, 10, 11, 12
    uint8_t Subchunk1ID[4]; // bytes 13, 14, 15, 16
    uint32_t Subchunk1Size; // bytes 17, 18, 19, 20
    uint16_t AudioFormat;   // bytes 21, 22
    uint16_t NumChannels;   // bytes 23, 24
    uint32_t SampleRate;    // bytes 25, 26, 27, 28
    uint32_t ByteRate;      // bytes 29, 30, 31, 32
    uint16_t BlockAlign;    // bytes 33, 34
    uint16_t BitsPerSample; // bytes 35, 36
    uint8_t Subchunk2ID[4]; // bytes 37, 38, 39, 40
    uint32_t Subchunk2Size; // bytes 41, 42, 43, 44
} WavHeader;



namespace md  // miniDart alias
{
    enum AudioDeviceState
    {
        AUDIO_DEVICE_SELECTED_AND_VALID        =  1 << 0,
        AUDIO_DEVICE_NO_RECORD_CAPABILITY      =  1 << 1,
        AUDIO_DEVICE_WITH_RECORDING_CAPABILITY =  1 << 2,
        AUDIO_DEVICE_PLAYBACK                  =  1 << 3,
        AUDIO_DEVICE_NO_STATE                  =  1 << 4,
        AUDIO_DEVICE_ERROR                     =  1 << 5,
        AUDIO_DEVICE_NONE                      =  1 << 6
    };

    // a given device
    class AudioDevice
    {
      public:
        // Ctor
        AudioDevice();
        // Dtor
        ~AudioDevice();

        // SDL_GetNumAudioDevices( SDL_TRUE) will return the number
        // of audio devices, but not sure they are recordable => init() will check
        bool              init();
        void              cleanAndClose();

        // will probably use :
        // SDL_GetAudioDeviceName(i, SDL_TRUE) associated to name.str().cstr();
        //std::string   getName(void);

        static void       audioRecordingCallback(void* userdata, Uint8* stream, int len);
        static void       audioPlaybackCallback (void* userdata, Uint8* stream, int len);

        bool              createRecordingSpecs(int);
        bool              createDeviceSpecs(int);
        bool              createRecordingCallback(int);
        bool              createPlaybackCallback(int);
        bool              audioDeviceEnable(int, bool);
        bool              audioDeviceDisable(int, bool);

        int               getRecordingDeviceID(void);
        int               getPlaybackDeviceID(void);

        std::string       aName;

        SDL_AudioSpec     s_wanted;
        SDL_AudioSpec     s_have;

        // actualise la liste de périphériques son
        // does SDL allow that :  bool resetDevice() 
        void              setRecordingBuffer();
        void              setPlaybackBuffer();

        bool              isSelected;
        bool              isRecordable;
        bool              isPlayback;
        bool              b_has_recording_specs;
        bool              b_has_recording_callback;
        bool              b_has_playback_callback;

        // visibility ?
        // IMPORTANT : variables declared as static in the interface.
        // BUT they MUST be redeclared in the implementation type+initialisation.
        // Else, the compilation FAILS

        static uint8_t *  audioRecordingBuffer;
        static uint32_t   audioBufferByteSize;
        static uint32_t   audioBufferBytePosition;
        static uint32_t   audioBufferByteReadPosition;
        static uint32_t   audioBufferByteMaxPosition;

        SDL_RWops *       rwAudio;
        FILE *            wavFile;
        std::string       wavFileName;
        struct            WavHeader wav_h;
        uint32_t          fileSize;
        uint32_t          ncount;
        bool              initWavFile(void);
        bool              b_wavFileCreated;
        int               closeWavFile(void);
        int               completeWavFile(void);
        int               createWavHeader();
        bool              finalizeWavHeader(void);

        inline void       setWavFileName(std::string sFileName) { wavFileName = sFileName; }

        inline bool       do_quit(void) { return b_quit; }
        inline void       set_quit(bool aValue) { b_quit = aValue; }

        // LATER use
        // void           reset(void);
        int               recordWavFile(void);
        int               recordRaw(void);
        void              playback(void);

        bool              b_recording;
        bool              b_playback;
        bool              b_paused;
        bool              createCallback(bool);

        inline bool       isCurrentDeviceRecording(void) { return b_recording_audio; }
        inline void       setRecording(bool b_record) { b_recording_audio = b_record; }

      private:
        bool              b_quit;
        bool              b_recording_audio;
        SDL_AudioDeviceID recordingAudioDevID;
        SDL_AudioDeviceID playbackAudioDevID;

        std::string       currentRecordableAudioDeviceName;
        std::string       s_name;
        int               currentState;

    }; /* class AudioDevice */


    // A vector of audioDevices, one can (un)select and start/stop recording
    // This object is meant to make the link with SDL2 audio and the system.
    // Reinit possible ?


    class AudioDevicesList
    {
      public:
        // Ctor
        AudioDevicesList();

        // Dtor
        ~AudioDevicesList();

        bool          init();
        void          clean_and_close();


        // audiodDevice recordable ?
        int           addAudioDevice();

        AudioDevice currentAudioDevice;

    }; /* class AudioDevicesList */


} // namespace md;


#endif /* _AUDIO_DEVICE_HPP */
