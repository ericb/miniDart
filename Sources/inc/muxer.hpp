/*
 *  File muxer.h belongs to miniDart project
 *
 */

//             FULL COPYRIGHT STORY

// 1. The muxer part has originaly been created by Copyright (c) 2003 Fabrice Bellard under MIT like license, as "muxing.c",
//    who belongs to ffmpeg source code. Factualy, a big part of ffmpeg_mux.cpp was initially written by Fabrice Bellard (ffmpeg author).

/*
 * Copyright (c) 2003 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/* 
 * 2. Nabil Dehaini in opencv_2_ffmpeg_av project, adapted this file to cv::Mat images and created (undefined license) :
 *     ffmpeg_mux class + all the interface, and he wrote :
 *
 *     - get_video_frame_cv(OutputStream* ost, cv::Mat* opencv_frame)
 *     - write_video_frame_cv(AVFormatContext* oc, OutputStream* ost, cv::Mat* opencv_frame)
 *     - allocate_buffer(OutputStream *ost)
 *
 *     See : https://github.com/ndehaini/opencv_2_ffmpeg_av  (read ffmpeg_mux.cpp)
 *-
 */


/*
 * 
 * 3. Eric Bachard (me) wrote : encode(AVCodecContext *avctx, AVPacket *pkt, AVFrame *frame, int *got_packet)
 *    The goal was to replace avcodec_encode_audio2() and avcodec_encode_video2() with something more modern 
 *    and coming from libavcodec API. Last, it was adapted to miniDart + SDL2 audiomanager + audiodevice
 *
 *
 *    To simplify things, encode() is copyright Eric Bachard (C) 2021, and is licensed under MIT license
 *
 */



 // 4.   (who's the next ?)

/*
 *  New muxer implementation, based on ffmpag-cpp2 inheritance. All methods are the one I wrote in the demos
 *  + full implementation to come (mostly based on the following demos : remux_webcam + filter audio + filter video
 *  Author and copyright : Eric Bachard  2021 August 27th 
 */

#ifndef MUXER_HPP
#define MUXER_HPP

#include "common.h"
#include <iostream>
#include <chrono>
#ifdef USE_FFMPEG_CPP2
#include <ffmpegcpp.h>
#endif
#include <thread>
#include <fstream>
#include <string>

enum Container
{
    MP4_CONTAINER     = 0,
    AVI_CONTAINER     = 1,
    MKV_CONTAINER     = 2,
    WEBM_CONTAINER    = 3,
    NOT_A_CONTAINER   = 100
};

enum VideoCodec
{
    DIVX_VIDEO_CODEC  = 0,
    DIV3_VIDEO_CODEC  = 1,
    DX50_VIDEO_CODEC  = 2,
    XVID_VIDEO_CODEC  = 3,
    MJPG_VIDEO_CODEC  = 4,
    H264_VIDEO_CODEC  = 5,
    VP8_VIDEO_CODEC   = 6,
    VP9_VIDEO_CODEC   = 7,
    FLV1_VIDEO_CODEC  = 8,
    U263_VIDEO_CODEC  = 9,
    THEO_VIDEO_CODEC  = 10,
    X262_VIDEO_CODEC  = 11,
    NOT_A_VIDEO_CODEC = 100
};

enum AudioCodec
{
    AAC_AUDIO_CODEC    = 0,
    AC3_AUDIO_CODEC    = 1,
    MP3_AUDIO_CODEC    = 2,
    DAT_AUDIO_CODEC    = 3,
    OPUS_AUDIO_CODEC   = 4,
    VORBIS_AUDIO_CODEC = 5,
    NOT_AN_AUDIO_CODEC = 100
};

//using namespace ffmpegcpp;

#ifdef USE_FFMPEG_CPP2
namespace md // miniDart alias
{
    class muxer : public ffmpegcpp::Muxer
    {

        public:
            muxer(const std::string & filename);
            virtual ~muxer(){};

            int init();
            int finish();
            int mux();

            std::string container;
            //std::string out_fileName;

            int audioCodec;
            int videoCodec;

            void setOutFileName(std::string);

            // Video pipeline
            void setVideoCodec(int);
            void createVideoContainer(std::string);
            void createVideoEncoder();
            void createVideoDemuxer();

            // Audio pipeline
            void setAudioCodec(int);
            void createAudioContainer(std::string);
            void createAudioEncoder();
            void createAudioFilter();
            void createAudioDemuxer();

            void create_final_video();

            av_common *common;
            int process_mux();
            bool keepcapturing;

            video_stream *video_reader1;
            audio_stream *audio_reader1;

            uint8_t *sound_buff;

            bool mux_audio;
            bool mux_video;

        private:

            int reset_pts;
            uint8_t *frame_buf;
            size_t buffsize;

            OutputStream video_st;
            OutputStream audio_st;

            const AVOutputFormat *fmt;
            AVFormatContext *oc;
            AVCodec *audio_codec, *video_codec;
            AVDictionary *opt;

            int have_video;
            int have_audio;
            int encode_video;
            int encode_audio;

            int encode(AVCodecContext *avctx, AVPacket *pkt, AVFrame *frame, int *got_packet);

            void      wait_signal();

            int       allocate_buffer(OutputStream *ost);
            int       deallocate_buffer();

            void      close_stream(AVFormatContext *oc, OutputStream *ost);

            void      open_video(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg);
            int       write_video_frame_cv(AVFormatContext *oc, OutputStream *ost, cv::Mat *opencv_frame);    

            AVFrame * get_video_frame_cv(OutputStream *ost, cv::Mat *opencv_frame);
            AVFrame * alloc_picture(enum AVPixelFormat pix_fmt, int width, int height);

            AVFrame * get_audio_frame_ex(OutputStream *ost, audio_stream *audio);
            AVFrame * alloc_audio_frame(enum AVSampleFormat sample_fmt, uint64_t channel_layout, int sample_rate, int nb_samples);

            void      open_audio(AVFormatContext *oc, AVCodec *codec, OutputStream *ost, AVDictionary *opt_arg);
            int       write_audio_frame_ex(AVFormatContext *oc, OutputStream *ost, AVFrame *avframe);

            void      add_stream(OutputStream *ost, AVFormatContext *oc, AVCodec **codec, enum AVCodecID codec_id);
            int       write_frame(AVFormatContext *fmt_ctx, const AVRational *time_base, AVStream *st, AVPacket *pkt);
            void      do_mux(audio_stream *audio_reader, video_stream *video_reader);    

            /////////////////This is the mother of all things here.///////////
            int mux_ex(audio_stream *audio_reader, video_stream *video_reader);
    };

} /* namespace md */

#endif /* USE_FFMPEG_CPP2 */

#endif /* MUXER_HPP */
