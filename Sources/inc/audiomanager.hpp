/*
 * File audiomanager.cpp belongs to miniDart project
 * Copyright : Eric Bachard  / 2019 July 7th, 14:35:03 (UTC+0200)
 * This document is under GPL v2 license
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <cstdint>
#include <cstring>
#include <string>
#include <vector>
#include <thread>
#include <SDL2/SDL.h>

#include "audiodevice.hpp"

#ifndef _AUDIO_MANAGER_HPP
#define _AUDIO_MANAGER_HPP

// Everything is based on the recently improved SDL_API : https://wiki.libsdl.org/CategoryAudio
// SDL_GetAudioDeviceName(i, 0) => no recording capability
// SDL_GetAudioDeviceName(i, 1) => recording capability  // this is the important information

// SDL2 API PROPOSES : 
//     SDL_NewAudioStream
//     SDL_AudioStreamPut
//     SDL_AudioStreamAvailable
//     SDL_AudioStreamGet
//     SDL_AudioStreamFlush
//     SDL_AudioStreamClear
//     SDL_FreeAudioStream

// https://wiki.libsdl.org/SDL_LockAudioDevice
// https://wiki.libsdl.org/SDL_UnlockAudioDevice

// audio devices.hpp // is 8 sufficient  ??  (currently testing 8 with miniDart,
// and got NULL for number 6 and number 7 (values starting from 0)
#define MAX_RECORDING_DEVICES  8
// be consistent with SDL_AUDIO_MIN_BUFFER_SIZE (ffmpeg_player.hpp)
// try 4 or 8 seconds (multiple of 2 ?)
#define RECORDER_BUFFER_DEFAULT_SECONDS  2

const int RECORDING_BUFFER_TIME = RECORDER_BUFFER_DEFAULT_SECONDS + 1;



namespace md  // miniDart alias
{

    // A vector of audioDevices, one can (un)select and start/stop recording
    // This object is meant to make the link with SDL2 audio and the system.
    // Reinit possible ?


    class AudioManager
    {
        public:

            // Ctor
            AudioManager();
            // Dtor
            ~AudioManager();

            bool init();

            void cleanAndClose(void);

            short int     createRecordableAudioDeviceNamesList(void);
            //std::vector<std::string> recordableSDLAudioDeviceNames;
            bool          selectSDLRecordableAudioDevice(short int);

            std::string   getCurrentSDLRecordableAudioDeviceName(void);
            int           getCurrentSDLRecordableAudioDevice(void);

            // FIXME : needs some work, starting reading the libav documentation + find code samples
            // audiodevices::toFFMPEG()  // need some love

            // a vector containing all the current detected audiodevices
            std::vector <AudioDevice > currentAudioDevicesList;

            // a vector containing all the current recordable audiodevices
            std::vector <std::string> recordableSDLAudioDeviceNames;

            // getVectorSize() returns audioDevicesVector.size();
            int           getVectorSize();
            bool          setDriverName(char * name);
            //char *        getDriverName();

            const char *        driver_name;

            int           count;

            // helper to select a given sound device
            bool          selectDevice(AudioDevicesList currentAudioDevicesList, int aNumber);

            int           getCurrentSelectedDevice(void);

            void          setCurrentRecordableDevice(int);
            void          startRecording(void);//int currentRecordingAudioDevice);
            void          startPlayback(void);

            void          startPause(void);
            void          stopPause(void);

            void          stopRecording(void);
            void          stopPlayback(void);

            int           nbrRecordingDevicesCount;  // private ?

        private:
            int           currentSDLSelectedDevice;
            AudioDevice   currentSDLRecordableAudioDevice;
    };
} // namespace md;


#endif /* _AUDIO_MANAGER_HPP */
