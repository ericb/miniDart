/*
 * Fichier d'en tête videoreader.hpp pour le projet miniDart
 * Auteur : Eric Bachard  / samedi 31 juillet 2021, 20:55:35 (UTC+0200)
 * Ce document est sous Licence GPL v2
 * voir : http://www.gnu.org/licenses/gpl-2.0.html
 */

#ifndef __VIDEO_MANAGER_H
#define __VIDEO_MANAGER_H

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <iostream>
#include <string>
#include <cstdio>
//#include <vector>

// kept because we'll have to use path to store the videos / audio files
#if defined ( __Linux__ )
#define MAX_PATH 512
#include <limits.h>
#endif

#ifdef _WIN32
#include <windows.h>
#ifndef PATH_MAX
#define PATH_MAX MAX_PATH
#endif
#endif

#include "application.hpp"
#include "imgui.h"
#include "pick_folder.h"

namespace md
{
    class VideoManager
    {
        public:
            // Ctor
            VideoManager();
            // Dtor
            ~VideoManager();

            bool init(void);

            void quit(void);

            cv::VideoWriter oVideoWriter;

            int startRecordingVideo();
            int stopRecordingVideo();

            int startPausingVideo();
            int stopPausingVideo();

            float initialize_position(void);

            int LoadFile(const char *, int);
            void do_seek(double, long int);

            char * currentPath;

            int selectFolder(const char *);

            unsigned int myFourcc;

            bool b_recording_video;
            bool b_full_video_selected;
            bool b_cell_phone_selected;
            bool b_video_running;
            bool b_video_playing_at_fps;
            bool b_open_a_file;

            std::string myContainer;
            int outWidth = 1280;
            int outHeight = 720;
            cv::Size  outFrameSize;
            double outFPS;

            float position;

            void inc_color();
            int aValue;

            bool b_use_USB_webcam;

#ifdef FPS_TEST
            int  fps_test;
#endif

#ifdef FPS_FEATURE_ENABLED
            int out_image_format; // IMAGE_FORMAT_720P
            int out_old_framesize;
            int out_new_framesize;
#endif
            int current_source;
            int old_current_source;

            char defaultPath[];
        private:


    };
} /* namespace md */


#endif  /*__VIDEO_READER_H */
