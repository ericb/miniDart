/*
 * File audiodevice.cpp belongs to miniDart project
 * Copyright : Eric Bachard  / 2019 april 26th, 14:35:03 (UTC+0200)
 * This document is under GPL v2 license
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include "application.hpp"
#include "audiodevice.hpp"
#include <vector>
#include <cstring>
#include "imgui.h"
#include "ffmpeg_player.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_thread.h>

//#ifndef DEBUG_AUDIO
//#define DEBUG_AUDIO
//#endif

// Ctor
md::AudioDevice::AudioDevice()
{
};

// Dtor
md::AudioDevice::~AudioDevice()
{
    cleanAndClose();
};

uint8_t *     md::AudioDevice::audioBuffer;


// visibility ?
uint8_t *     md::AudioDevice::audioRecordingBuffer;
uint32_t      md::AudioDevice::audioBufferByteSize;  // taille du buffer audio en bytes. Dépend du nombre max de samples
uint32_t      md::AudioDevice::audioBufferBytePosition; // IN position en écriture
uint32_t      md::AudioDevice::audioBufferByteReadPosition; // OUT position en lecture 
uint32_t      md::AudioDevice::audioBufferByteMaxPosition; // fin du buffer en bytes


bool md::AudioDevice::init()
{
    b_recording = false;
    b_playback  = false;
    b_paused = false;
    ncount      = 0;
    b_wavFileCreated = false;

    // doing that, we keep the possibility to choose the filename
    wavFileName = "audiofile.wav";

    isRecordable                     = 0;
    b_has_playback_callback          = false;
    b_has_recording_callback         = false;
    b_has_recording_specs            = false;

    recordingAudioDevID              = 0;
    playbackAudioDevID               = 0;
    currentRecordableAudioDeviceName = "";  // default device

    audioRecordingBuffer             = 0;
    audioBufferByteSize              = 0;
    audioBufferBytePosition          = 0;
    audioBufferByteReadPosition      = 0;
    audioBufferByteMaxPosition       = 0;

    return true;
}

void md::AudioDevice::cleanAndClose()
{
}


bool md::AudioDevice::createPlaybackCallback(int aDevice)
{
    if (b_has_playback_callback)
        return true;

    // define audio specs containers
    SDL_AudioSpec wantedPlaybackSpec;
    SDL_AudioSpec havePlaybackSpec;

    // playback prototype.May differ from the recording callback
    auto set_playback_audio_spec_S32 = [this](SDL_AudioSpec &s, SDL_AudioCallback c)
    {
        // TODO : use better values. Probably reduce the buffer size, to decrease the delay
        SDL_zero(s);
        s.freq = 44100;
        s.format = AUDIO_S32; // AUDIO_S16SYS;//
        s.channels = 2 ;  // stereo
        s.silence = 0;
        s.samples = SDL_AUDIO_MIN_BUFFER_SIZE * 2; // 2048;  /!\ Less could be better, to avoid a hole in the last buffer plot
        s.callback = c;
        s.userdata = this;
    };

    set_playback_audio_spec_S32(wantedPlaybackSpec, md::AudioDevice::audioPlaybackCallback);
#ifdef DEBUG_AUDIO
    std::cerr << ">>>>  set_playback_audio_spec done " << "\n";
#endif
    aName = SDL_GetAudioDeviceName(aDevice, SDL_TRUE);
#ifdef DEBUG_AUDIO
    std::cerr << ">>>> SDL_GetAudioDeviceName(aDevice, SDL_FALSE) done, and aName contains :  " << aName.c_str() << "\n";
#endif
    playbackAudioDevID = SDL_OpenAudioDevice(aName.c_str(),
                                          SDL_FALSE,
                                          &wantedPlaybackSpec, &havePlaybackSpec,

                                          SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);

#ifdef DEBUG_AUDIO
    std::cerr << ">>>>  Opening : " << aName.c_str() << "\n";
    std::cerr << "aDevice         = " << aDevice << "\n";
    std::cerr << "playbackAudioDevID = " << playbackAudioDevID << "\n";
#endif
    if (playbackAudioDevID == 0)
    {
        std::cerr << "[audio]could not open audio, error:" << SDL_GetError() << std::endl;
        return false;
    }

    //Calculate per sample bytes
    int bytesPerSample = havePlaybackSpec.channels * ( SDL_AUDIO_BITSIZE( havePlaybackSpec.format ) / 8 );

    //Calculate bytes per second
    int bytesPerSecond = havePlaybackSpec.freq * bytesPerSample;

    //Calculate buffer size
    md::AudioDevice::audioBufferByteSize = MAX_RECORDING_SECONDS * bytesPerSecond;

#ifdef DEBUG_AUDIO
    fprintf(stderr, "md::AudioDevice::audioBufferBytePosition     = %d \n", md::AudioDevice::audioBufferBytePosition);
    fprintf(stderr, "bytesPerSample              = %d \n", bytesPerSample);
    fprintf(stderr, "bytesPerSecond              = %d \n", bytesPerSecond);
    fprintf(stderr, "md::AudioDevice::audioBufferByteSize         = %d \n", md::AudioDevice::audioBufferByteSize);
    fprintf(stderr, "md::AudioDevice::audioBufferByteMaxPosition  = %d \n", md::AudioDevice::audioBufferByteMaxPosition);
#endif
    // initialize
    md::AudioDevice::audioBufferBytePosition = 0;

    b_has_playback_callback = true;

    return  true;

}


bool md::AudioDevice::createRecordingCallback(int aDevice)
{
    if (b_has_recording_callback)
        return true;

    // define audio specs containers
    SDL_AudioSpec wanted;
    SDL_AudioSpec have;

    // prototype
    auto set_record_audio_spec_S32 = [this](SDL_AudioSpec &s, SDL_AudioCallback c)
    {
        SDL_zero(s);
        s.freq = 44100;
        s.format = AUDIO_S32; // AUDIO_S16SYS;//
        s.channels = 2 ;  // stereo
        s.silence = 0;
        s.samples = SDL_AUDIO_MIN_BUFFER_SIZE * 2; // 2048;  /!\ Less could be better, to avoid a hole in the last buffer plot
        s.callback = c;
        s.userdata = this;
    };

    set_record_audio_spec_S32(wanted, md::AudioDevice::audioRecordingCallback);
    aName = SDL_GetAudioDeviceName(aDevice, SDL_TRUE);

    recordingAudioDevID = SDL_OpenAudioDevice(aName.c_str(),
                                          SDL_TRUE,
                                          &wanted, &have,
                                          SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE);

#ifdef DEBUG_AUDIO
    std::cerr << ">>>>  Opening : " << aName.c_str() << "\n";
    std::cerr << "aDevice         = " << aDevice << "\n";
    std::cerr << "recordingAudioDevID = " << recordingAudioDevID << "\n";
#endif
    if (recordingAudioDevID == 0)
    {
        std::cerr << "[audio]could not open audio, error:" << SDL_GetError() << std::endl;
        return false;
    }

    //Calculate per sample bytes
    int bytesPerSample = have.channels * ( SDL_AUDIO_BITSIZE( have.format ) / 8 );

    //Calculate bytes per second
    int bytesPerSecond = have.freq * bytesPerSample;

    //Calculate buffer size
    md::AudioDevice::audioBufferByteSize = MAX_RECORDING_SECONDS * bytesPerSecond;

    //Calculate max buffer use
    md::AudioDevice::audioBufferByteMaxPosition = RECORDING_BUFFER_SECONDS * bytesPerSecond;

#ifdef DEBUG_AUDIO
    fprintf(stderr, "audioBufferBytePosition     = %d \n", md::AudioDevice::audioBufferBytePosition);
    fprintf(stderr, "bytesPerSample              = %d \n", bytesPerSample);
    fprintf(stderr, "bytesPerSecond              = %d \n", bytesPerSecond);
    fprintf(stderr, "audioBufferByteSize         = %d \n", md::AudioDevice::audioBufferByteSize);
    fprintf(stderr, "audioBufferByteMaxPosition  = %d \n", md::AudioDevice::audioBufferByteMaxPosition);
#endif
    setRecordingBuffer();

    b_has_recording_callback = true;

    return  true;
}

bool md::AudioDevice::createDeviceSpecs(int selectedDevice)
{
    bool toReturn = false;

#ifdef DEBUG_AUDIO
    std::cerr << "Inside createDeviceSpecs. b_has_recording_specs = " << b_has_recording_specs << "\n";
#endif
    if (b_has_recording_specs == true)
    {
#ifdef DEBUG_AUDIO
        std::cerr << "recording specs already created" << "\n";
#endif
        toReturn = true;
    }
    else
    {
        toReturn = createRecordingSpecs(selectedDevice);

        if (false == toReturn)
            std::cerr << "Problem with recording callback creation" << "\n";
    }

    return toReturn;
}

bool md::AudioDevice::createRecordingSpecs(int aDevice)
{
#ifdef DEBUG_AUDIO
    std::cerr << "/!\\ >>>>  in md::AudioDevice::createRecordingSpecs(int aDevice) " << "\n";
#endif
    if (b_has_recording_specs)
        return false;

    // prototype
    auto set_record_audio_spec_S16LSB = [this](SDL_AudioSpec &s, SDL_AudioCallback c)
    {
        SDL_zero(s);
        s.freq = 44100;
        s.format = AUDIO_S16LSB; //AUDIO_S32; // AUDIO_S16SYS;//
        s.channels = 2 ;  // stereo
        s.silence = 0;
        s.samples = 4096; // 2048;  /!\ Less could be better, to avoid a hole in the last buffer plot
        s.callback = nullptr;
        s.userdata = this;
    };

    set_record_audio_spec_S16LSB(s_wanted, nullptr);
    aName = SDL_GetAudioDeviceName(aDevice, SDL_TRUE);

    recordingAudioDevID = SDL_OpenAudioDevice(aName.c_str(),
                                          SDL_TRUE,
                                          &s_wanted, &s_have,
                                          0/*SDL_AUDIO_ALLOW_FREQUENCY_CHANGE | SDL_AUDIO_ALLOW_CHANNELS_CHANGE*/);

#ifdef DEBUG_AUDIO
    std::cerr << ">>>>  Opening : "       << aName.c_str() << "\n";
    std::cerr << "aDevice         = "     << aDevice << "\n";
    std::cerr << "recordingAudioDevID = " << recordingAudioDevID << "\n";
    std::cerr << "s_have.freq     = "     << s_have.freq << "\n";
    std::cerr << "s_have.format   = "     << s_have.format << "\n";
    std::cerr << "s_have.channels = "     << (int)s_have.channels << "\n";
    std::cerr << "s_have.samples  = "     << s_have.samples << "\n";
#endif
    if (recordingAudioDevID == 0)
    {
        std::cerr << "[audio]could not open audio, error:" << SDL_GetError() << std::endl;
        return false;
    }

    // only once
    b_has_recording_specs = true;

#ifdef DEBUG_AUDIO
    std::cerr << "First time there. b_has_recording_specs  = "     << b_has_recording_specs << "\n";

    //Calculate per sample bytes
    int bytesPerSample = s_have.channels * (SDL_AUDIO_BITSIZE( s_have.format ) / 8);

    //Calculate bytes per second
    int bytesPerSecond = s_have.freq * bytesPerSample;

    fprintf(stderr, "bytesPerSample              = %d \n", bytesPerSample);
    fprintf(stderr, "bytesPerSecond              = %d \n", bytesPerSecond);
#endif
    return  true;
}

bool md::AudioDevice::audioDeviceEnable(int aDevice, bool recordable)
{
    return true;
}

bool md::AudioDevice::audioDeviceDisable(int aDevice, bool recordable)
{
    return true;
}


int md::AudioDevice::getRecordingDeviceID()
{
    return recordingAudioDevID;
}

int md::AudioDevice::getPlaybackDeviceID()
{
    return playbackAudioDevID;
}


void md::AudioDevice::setRecordingBuffer()
{
    // initialize
    audioBufferBytePosition = 0;

    //Allocate and initialize byte buffer everytime we change the source
    // first free the previous buffer, if existing
    if (audioRecordingBuffer != 0)
        free(audioRecordingBuffer);

    audioRecordingBuffer = (uint8_t *)malloc(audioBufferByteSize);
    memset(audioRecordingBuffer, 0, audioBufferByteSize );

#ifdef DEBUG_AUDIO
    fprintf(stderr, "Adresse de audioRecordingBuffer         = %p \n", &audioRecordingBuffer);
    fprintf(stderr, "sizeof(audioRecordingBuffer)            = %ld \n", sizeof(audioRecordingBuffer));
#endif
}

void md::AudioDevice::setPlaybackBuffer()
{
    // initialize
    audioBufferBytePosition = 0;
}

void md::AudioDevice::audioRecordingCallback(void* userdata, Uint8* stream, int len)
{
#ifdef DEBUG_AUDIO
    fprintf(stderr, "Using callback at %u\n", SDL_GetTicks());
    fprintf(stderr, "audioRecordingBuffer = %d \n", audioBufferBytePosition);

    std::cerr << "Recording. audioBufferBytePosition = " << audioBufferBytePosition << "\n";
#endif
    //Move along buffer
    if (audioBufferBytePosition + len >= audioBufferByteSize)
    {
        audioBufferBytePosition = 0;
        return;
    }
    else
    {
        //Copy audio from stream
        memcpy(&audioRecordingBuffer[audioBufferBytePosition], stream, len);
        audioBufferBytePosition += len;
    }
}


void md::AudioDevice::audioPlaybackCallback(void* userdata, Uint8* stream, int len)
{
#ifdef DEBUG_AUDIO
    fprintf(stderr, "Using callback at %u\n", SDL_GetTicks());
    fprintf(stderr, "position for playback = %d \n", audioBufferByteReadPosition);
#endif
    //Copy audio to stream
    memcpy(stream, &audioRecordingBuffer[audioBufferByteReadPosition], len);

    //Move along buffer
    audioBufferByteReadPosition += len;

    if (audioBufferByteReadPosition + len >= audioBufferByteSize)
    {
        audioBufferByteReadPosition = 0;
        return;
    }

#ifdef DEBUG_AUDIO
    fprintf(stderr, "After len incrementation, ");
    fprintf(stderr, "audioPlaybackBuffer position = %d \n", audioBufferByteReadPosition);
#endif
}

int  md::AudioDevice::createWavHeader()
{
    wav_h.FileID[0] = 'R';
    wav_h.FileID[1] = 'I';
    wav_h.FileID[2] = 'F';
    wav_h.FileID[3] = 'F';
    wav_h.Format[0] = 'W';
    wav_h.Format[1] = 'A';
    wav_h.Format[2] = 'V';
    wav_h.Format[3] = 'E';
    wav_h.Subchunk1ID[0] = 'f';
    wav_h.Subchunk1ID[1] = 'm';
    wav_h.Subchunk1ID[2] = 't';
    wav_h.Subchunk1ID[3] = ' ';
    wav_h.Subchunk2ID[0] = 'd';
    wav_h.Subchunk2ID[1] = 'a';
    wav_h.Subchunk2ID[2] = 't';
    wav_h.Subchunk2ID[3] = 'a';
    wav_h.NumChannels = 2; // stereo
    wav_h.BitsPerSample = 16;
    wav_h.Subchunk2Size = 300 * MAX_SAMPLES * (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;
    wav_h.FileSize = (uint32_t) wav_h.Subchunk2Size + 36;
    wav_h.Subchunk1Size = 16;
    wav_h.AudioFormat = 1; // 1 is PCM, 2 is byte integer (unsupported on Linux)
    wav_h.SampleRate = 44100; // taken from the specs
    wav_h.ByteRate = (uint32_t) wav_h.SampleRate
                     * (uint32_t) wav_h.NumChannels
                     * (uint32_t) wav_h.BitsPerSample / 8;
    wav_h.BlockAlign = (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;

    return EXIT_SUCCESS;
}


bool md::AudioDevice::initWavFile()
{
    if (b_wavFileCreated == true)
        return true;

    wavFile = fopen(wavFileName.c_str(), "wb");

    if (wavFile != nullptr)
    {
        createWavHeader();
        fwrite(&wav_h, 1, sizeof(wav_h), wavFile);
    }
    else
    {
        std::cerr << "cannot open wav file to write data" << "\n";
        return false;
    }

    rwAudio = SDL_RWFromFile(wavFileName.c_str(), "a+");

    b_wavFileCreated = true;
    return true;
}

int md::AudioDevice::closeWavFile(void)
{
    if (wavFile != nullptr)
        fclose(wavFile);
    else
    {
        std::cerr << "wav file already closed" << "\n";
        return FCLOSE_ERROR;
    }
    SDL_RWclose(rwAudio);

    b_wavFileCreated = false;

    return EXIT_SUCCESS;
}


int md::AudioDevice::completeWavFile(void)
{
    finalizeWavHeader();

    return EXIT_SUCCESS;
}

bool md::AudioDevice::finalizeWavHeader(void)
{
    wav_h.Subchunk2Size = MAX_BUF_SIZE * 4 * ncount * (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;
    wav_h.FileSize = (uint32_t) wav_h.Subchunk2Size + 36;

#ifdef DEBUG_AUDIO
    std::cerr << "ncount              =  " <<  ncount  << "\n";
    std::cerr << "s_wav_h.SampleRate    = "  <<  wav_h.SampleRate << "\n";
    std::cerr << "s_wav_h.Subchunk2Size = "  <<  wav_h.Subchunk2Size << "\n";
    std::cerr << "s_wav_h.FileSize     = "  <<  wav_h.FileSize     << "\n";
#endif
    SDL_RWseek(rwAudio, 0, RW_SEEK_SET);

    // avoid potential crash
    if (wavFile != nullptr)
    {
        fwrite(&wav_h, 1, sizeof(wav_h), wavFile);
    }

#ifdef DEBUG_AUDIO
    std::cerr << "audiofile.wav header finalized"   << "\n";
#endif
    ncount = 0;
    return true;
}

int md::AudioDevice::recordRaw(void)
{
    // FIXME : implement me
    // select Audio recordable stream
    // provide audio frame
    // send to the demuxer, convert, and so on
    return EXIT_SUCCESS;
}

int md::AudioDevice::recordWavFile(void)
{
#ifdef DEBUG_AUDIO
    std::cerr << "wavFile =   " <<  wavFile << " and b_paused = " << b_paused << "\n";
#endif
    do
    {
        // /!\  MAX_BUF_SIZE * 4 bytes * 2 channels, else the audio file duration will be divided by 2
        char wav_data[MAX_BUF_SIZE * 8]; 

        const Uint32 br = SDL_DequeueAudio(getRecordingDeviceID(), wav_data, sizeof(wav_data));

        if (br == 0)
            std::cerr << "queue currently empty "  << "\n";

        if (wavFile != nullptr)
        {
            if (!b_paused)
                fwrite(&wav_data, 1, MAX_BUF_SIZE * 8, wavFile);
        }
        else
        {
            std::cerr << "cannot write data to file"  << "\n";
            return FOPEN_ERROR;
        }

        ncount++;

        // FIXME : without that, the recording is too fast, and only noisy
        SDL_Delay(45);

    } while (do_quit() == false);

    return EXIT_SUCCESS;
}


void md::AudioDevice::playback()
{
    // fill me
}

bool md::AudioDevice::createCallback(bool recordable)
{
    bool toReturn = false;

    if (recordable)
    {
        if (b_has_recording_callback)
        {
#ifdef DEBUG_AUDIO
            std::cerr << "recording callback already created" << "\n";
#endif
            toReturn = true;
        }
        else
        {
            toReturn = createRecordingCallback(getRecordingDeviceID());

            if (false == toReturn)
                std::cerr << "Problem with recording callback creation" << "\n";
        }
    }
    else
    {
        if (b_has_playback_callback)
        {
#ifdef DEBUG_AUDIO
            std::cerr << "playback callback already created" << "\n";
#endif
            toReturn = true;
        }
        else
        {
            toReturn = createPlaybackCallback(getRecordingDeviceID());

            if (false == toReturn)
                std::cerr << "Problem with playback callback creation" << "\n";
        }
    }
    return toReturn;
}



