/* 
 * file alsa_record.cpp, belongs to miniDart project
 * Copyright Eric Bachard 2020 August 23th   17:55:00   CEST
 * License : GPL v3
 * See: http://www.gnu.org/licenses/gpl-3.0.html
 */

// Linux only
#ifdef NATIVE_BUILD

#include "alsa_recorder.hpp"

// this is the bitrate
//#define MAX_BUF_SIZE	512
#define MAX_BUF_SIZE	1024
//#define MAX_BUF_SIZE	4096
//  8s
//#define MAX_SAMPLES	256000
#define MAX_SAMPLES	512000

// https://stackoverflow.com/questions/20944784/why-is-conversion-from-string-constant-to-char-valid-in-c-but-invalid-in-c
AlsaRecorder::AlsaRecorder() : fname("test.wav")
{
};


AlsaRecorder::~AlsaRecorder()
{
};

void AlsaRecorder::init()
{
    init_soundcard();
    init_wav_header();
    init_wav_file(fname);
    std::cout << "Open alsa_recorder and init done" << "\n";
}


/// Open and init default sound card params
int AlsaRecorder::init_soundcard()
{
    int err = 0;

    if ((err = snd_pcm_open(&capture_handle, snd_device, SND_PCM_STREAM_CAPTURE, 0)) < 0)

    {
        std::cerr << "cannot open audio device " << snd_device << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return OPEN_ERROR;
    }

    if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0)
    {
        std::cerr << "cannot allocate hardware parameter structure " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return MALLOC_ERROR;
    }

    if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0)
    {
        std::cerr << "cannot initialize hardware parameter structure " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return ANY_ERROR;
    }

    if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params,
                SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
        std::cerr << "cannot set access type " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return ACCESS_ERROR;
    }

    if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
    {
        std::cerr << "cannot set sample format " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return FORMAT_ERROR;
    }

    if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params,
                &srate, 0)) < 0)
    {
        std::cerr << "cannot set sample rate " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return RATE_ERROR;
    }

    if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, nchan))< 0)
    {
        std::cerr << "cannot set channel count " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return CHANNELS_ERROR;
    }

    if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0)
    {
        std::cerr << "cannot set parameters " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return PARAMS_ERROR;
    }

    if ((err = snd_pcm_prepare(capture_handle)) < 0)
    {
        std::cerr << "cannot prepare audio interface for use " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return PREPARE_ERROR;
    }

    if ((err = snd_pcm_start(capture_handle)) < 0)
    {
        std::cerr << "cannot start soundcard " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
        return START_ERROR;
    }

    return EXIT_SUCCESS;
}

int AlsaRecorder::close_soundcard()
{
    return snd_pcm_close(capture_handle);
}

int AlsaRecorder::init_wav_header()
{
    wav_h.ChunkID[0]     = 'R';
    wav_h.ChunkID[1]     = 'I';
    wav_h.ChunkID[2]     = 'F';
    wav_h.ChunkID[3]     = 'F';

    wav_h.Format[0]      = 'W';
    wav_h.Format[1]      = 'A';
    wav_h.Format[2]      = 'V';
    wav_h.Format[3]      = 'E';

    wav_h.Subchunk1ID[0] = 'f';
    wav_h.Subchunk1ID[1] = 'm';
    wav_h.Subchunk1ID[2] = 't';
    wav_h.Subchunk1ID[3] = ' ';

    wav_h.Subchunk2ID[0] = 'd';
    wav_h.Subchunk2ID[1] = 'a';
    wav_h.Subchunk2ID[2] = 't';
    wav_h.Subchunk2ID[3] = 'a';

    wav_h.NumChannels = nchan;
    wav_h.BitsPerSample = 16;
    wav_h.Subchunk2Size = 300 * MAX_SAMPLES * (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;
    //wav_h.Subchunk2Size = 0xFFFFFFFF;
    wav_h.ChunkSize = (uint32_t) wav_h.Subchunk2Size + 36;
    wav_h.Subchunk1Size = 16;
    wav_h.AudioFormat = 1;
    wav_h.SampleRate = srate;
    //wav_h.ByteRate = 96000;
    wav_h.ByteRate = (uint32_t) wav_h.SampleRate
                     * (uint32_t) wav_h.NumChannels
                     * (uint32_t) wav_h.BitsPerSample / 8;

    wav_h.BlockAlign = (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;

    return EXIT_SUCCESS;
}

int AlsaRecorder::init_wav_file(char * fname)
{
    fwav = fopen(fname, "wb");

    if (fwav != NULL)
        fwrite(&wav_h, 1, sizeof(wav_h), fwav);
    else
    {
        std::cerr << "cannot open wav file to write data" << "\n";
        return FOPEN_ERROR;
    }

    return EXIT_SUCCESS;
}

int AlsaRecorder::close_wav_file()
{
    if (fwav != NULL)
        fclose(fwav);
    else
    {
        std::cerr << "cannot close wav file" << "\n";
        return FCLOSE_ERROR;
    }

    return EXIT_SUCCESS;
}


int AlsaRecorder::do_record()
{
    uint32_t ncount = 0;
    char wav_data[MAX_BUF_SIZE * 4];
    int err = 0;

    do
    {
        if ((err = snd_pcm_readi(capture_handle, wav_data, MAX_BUF_SIZE)) != MAX_BUF_SIZE)
        {
            std::cerr << "read from audio interface failed " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";

            if (err == -32) // Broken pipe
            {
                if ((err = snd_pcm_prepare(capture_handle)) < 0)
                {
                    std::cerr << "cannot prepare audio interface for use " << "(" << snd_strerror(err) << ", " << err << ")" << "\n";
                    return PREPARE_ERROR;
                }
            }
            else
                return SNDREAD_ERROR;

        }

        if (fwav != NULL)
            fwrite(wav_data, 1, MAX_BUF_SIZE * 4, fwav);
        else
        {
            std::cerr << "cannot write data to file"  << "\n";
            return FOPEN_ERROR;
        }

        ncount++;

    } while (do_quit() == false);

    std::cout << "ncount : "  <<  ncount << "\n";
    wav_h.Subchunk2Size = MAX_BUF_SIZE * ncount * (uint32_t) wav_h.NumChannels * (uint32_t) wav_h.BitsPerSample / 8;
    wav_h.ChunkSize = (uint32_t) wav_h.Subchunk2Size + 36;
    std::cout << "wav_h.Subchunk2Size : "  <<  wav_h.Subchunk2Size << "\n";
    std::cout << "wav_h.ChunkSize     : "  <<  wav_h.ChunkSize     << "\n";
    fwrite(&wav_h, 1, sizeof(wav_h), fwav);

    return EXIT_SUCCESS;
}

#endif  /* NATIVE_BUILD */
