/*
 * File audiomanager.cpp belongs to miniDart project
 * Copyright : Eric Bachard  / 2019 april 26th, 14:35:03 (UTC+0200)
 * This document is under GPL v2 license
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include "application.hpp"
#include "audiomanager.hpp"
#include <vector>
#include <cstring>
#include <stdio.h>

#include "imgui.h"
#include "ffmpeg_player.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_thread.h>

// Debug only (costly)
//#ifndef DEBUG_AUDIO
//#define DEBUG_AUDIO
//#endif

#ifdef DEBUG_AUDIO
static void printStatus(SDL_AudioDeviceID dev)
{
    switch (SDL_GetAudioDeviceStatus(dev))
    {
        case SDL_AUDIO_STOPPED:
            fprintf(stderr, "stopped\n");
        break;

        case SDL_AUDIO_PLAYING:
            fprintf(stderr, "playing\n");
        break;

        case SDL_AUDIO_PAUSED :
            fprintf(stderr, "paused\n");
        break;

        default: fprintf(stderr, "???");
        break;
    }
}
#endif

md::AudioManager::AudioManager()
{
    currentSDLSelectedDevice = -1;
};


md::AudioManager::~AudioManager()
{
    cleanAndClose();
};


void md::AudioManager::cleanAndClose(void)
{
    while (currentAudioDevicesList.size() > 0)
    {
        currentAudioDevicesList.erase(currentAudioDevicesList.end());
#ifdef DEBUG_AUDIO
        std::cerr << "currentAudioDevicesList vector size : "<< currentAudioDevicesList.size() << "\n";
#endif
    }
    currentAudioDevicesList.clear();
}

bool md::AudioManager::init()
{
    count = 0;
#ifdef DEBUG_AUDIO
    std::cerr << "Found : " <<  "\n" << createRecordableAudioDeviceNamesList() << " recordable audio devices." << "\n";
#endif
    //Get capture device count
    nbrRecordingDevicesCount = SDL_GetNumAudioDevices(SDL_TRUE);

#ifdef DEBUG_AUDIO
    fprintf(stdout, "Number of Audio devices (with recording capability) %d \n",nbrRecordingDevicesCount);
#endif
    driver_name = SDL_GetCurrentAudioDriver();

    count = createRecordableAudioDeviceNamesList();
    selectSDLRecordableAudioDevice(DEFAULT_RECORDING_AUDIO_DEVICE);

    currentAudioDevicesList.at(currentSDLSelectedDevice).createDeviceSpecs(currentSDLSelectedDevice);

#ifdef DEBUG_AUDIO
    fprintf(stdout, "Audio driver name :  %s \n", driver_name);
#endif
    return true;
}

short int md::AudioManager::createRecordableAudioDeviceNamesList(void)
{
    while (SDL_GetAudioDeviceName(count, 1) != nullptr)
    {
        md::AudioDevice aDevice;
        aDevice.init();
        currentAudioDevicesList.push_back(aDevice);

#ifdef DEBUG_AUDIO
        std::cerr << "Audio device (with recording capability) device number : " << count  << " name : "  <<  SDL_GetAudioDeviceName(count, 1) << std::endl;
#endif
        count++;

        if (count > 4)
            break;
    }
    return count;
}


bool md::AudioManager::selectSDLRecordableAudioDevice(short int aDevice)
{
#ifdef DEBUG_AUDIO
    std::cerr << "Requested device : " << aDevice << "\n";
#endif
    if (aDevice >= count)
    {
        std::cerr << "Wrong device" << "\n";
        return false;
    }

#ifdef DEBUG_AUDIO
    static int value = getCurrentSDLRecordableAudioDevice();

    if (value >= 0)
    {
        std::cerr << "Previous device index was : " << getCurrentSDLRecordableAudioDevice()
                  << " with name : "                << SDL_GetAudioDeviceName(value, 1)
                  << "\n";
    }

    std::cerr << "Current status for previous device is : "; printStatus(getCurrentSDLRecordableAudioDevice());
    std::cerr << "New device requested : " << aDevice << "\n";
    std::cerr << "status of the requested device  : "; printStatus(aDevice);
#endif
    // here we realy define the recordable device
    setCurrentRecordableDevice(aDevice);

#ifdef DEBUG_AUDIO
    std::cerr << "currentRecordableAudioDevice is now : " << getCurrentSDLRecordableAudioDeviceName() << "\n";
    std::cerr << "Current status for new device is : "; printStatus(getCurrentSDLRecordableAudioDevice());
    std::cerr << "\n";
#endif
    return true;
}


int md::AudioManager::getCurrentSDLRecordableAudioDevice()
{
    return currentSDLSelectedDevice;
}

std::string md::AudioManager::getCurrentSDLRecordableAudioDeviceName()
{
    return SDL_GetAudioDeviceName(currentSDLSelectedDevice, 1);
}


int md::AudioManager::getCurrentSelectedDevice()
{
    return currentSDLSelectedDevice;
}

void md::AudioManager::setCurrentRecordableDevice(int aDevice)
{
    currentSDLSelectedDevice = aDevice;
}


void md::AudioManager::startPause(void)
{
    if (currentAudioDevicesList.at(currentSDLSelectedDevice).b_paused == false)
    {
        int aRecordingDeviceID = currentAudioDevicesList.at(currentSDLSelectedDevice).getRecordingDeviceID();

#ifdef DEBUG_AUDIO
        std::cerr << "aRecordingDeviceID = " << aRecordingDeviceID << " PAUSE STARTED ..." << "\n";
#endif
        SDL_PauseAudioDevice(aRecordingDeviceID, SDL_TRUE);
#ifdef DEBUG_AUDIO
        std::cerr << "Device " << currentSDLSelectedDevice<< " PAUSE STARTED ..." << "\n";
#endif
        currentAudioDevicesList.at(currentSDLSelectedDevice).b_paused = true;
    }
}

void md::AudioManager::stopPause(void)
{
    if (currentAudioDevicesList.at(currentSDLSelectedDevice).b_paused == true)
    {
        int aRecordingDeviceID = currentAudioDevicesList.at(currentSDLSelectedDevice).getRecordingDeviceID();

#ifdef DEBUG_AUDIO
        std::cerr << "aRecordingDeviceID = " << aRecordingDeviceID << " PAUSE STARTED ..." << "\n";
#endif
        SDL_PauseAudioDevice(aRecordingDeviceID, SDL_FALSE);
#ifdef DEBUG_AUDIO
        std::cerr << "Device " << currentSDLSelectedDevice<< " BACK TO RECORDING" << "\n";
#endif
        currentAudioDevicesList.at(currentSDLSelectedDevice).b_paused = false;

    }
}

void md::AudioManager::startRecording(void)
{
    if (false == currentAudioDevicesList.at(currentSDLSelectedDevice).b_recording)
    {
        if (currentAudioDevicesList.at(currentSDLSelectedDevice).b_wavFileCreated == false)
            currentAudioDevicesList.at(currentSDLSelectedDevice).initWavFile();

        SDL_PauseAudioDevice(currentAudioDevicesList.at(currentSDLSelectedDevice).getRecordingDeviceID(), SDL_FALSE);

#ifdef DEBUG_AUDIO
        std::cerr << "Device " << currentSDLSelectedDevice<< " RECORDING STARTED ..." << "\n";
#endif
        currentAudioDevicesList.at(currentSDLSelectedDevice).b_recording = true;
    }
}

void md::AudioManager::stopRecording(void)
{
    SDL_PauseAudioDevice(currentAudioDevicesList.at(currentSDLSelectedDevice).getRecordingDeviceID(), SDL_TRUE);
#ifdef DEBUG_AUDIO
    std::cerr << "Device " << currentSDLSelectedDevice << " RECORDING COMPLETED ....  " << "\n";
#endif
    currentAudioDevicesList.at(currentSDLSelectedDevice).completeWavFile();
    currentAudioDevicesList.at(currentSDLSelectedDevice).closeWavFile();
    currentAudioDevicesList.at(currentSDLSelectedDevice).b_recording = false;

#ifdef DEBUG_AUDIO
    std::cerr << "... done " << "\n";
#endif
}

void md::AudioManager::startPlayback(void)
{
    if (false == currentAudioDevicesList.at(currentSDLSelectedDevice).b_playback)
    {
        currentAudioDevicesList.at(currentSDLSelectedDevice).setPlaybackBuffer();
        SDL_PauseAudioDevice(currentAudioDevicesList.at(currentSDLSelectedDevice).getPlaybackDeviceID(), SDL_FALSE);
#ifdef DEBUG_AUDIO
        std::cerr << "Device " << currentSDLSelectedDevice << " PLAYBACK STARTED ....  " << "\n";
#endif
        currentAudioDevicesList.at(currentSDLSelectedDevice).b_playback = true;
    }
}

void md::AudioManager::stopPlayback(void)
{
    SDL_PauseAudioDevice(currentAudioDevicesList.at(currentSDLSelectedDevice).getPlaybackDeviceID(), SDL_TRUE);
#ifdef DEBUG_AUDIO
    std::cerr << "Device " << currentSDLSelectedDevice << " PLAYBACK COMPLETED ....  " << "\n";
#endif
    currentAudioDevicesList.at(currentSDLSelectedDevice).b_playback = false;
#ifdef DEBUG_AUDIO
    std::cerr << "... done " << "\n";
#endif
}


