-(Audio part of miniDart :

What we'd like to do there :


1.  [DONE] **DETECT** : detect SDL_AudioDevices : enumerate every recordable source

2.  [DONE] **DISPLAY** : Display a list of the current available sources

3.  [DONE] **SELECT** : select the source of data between the sound coming from a video, a remote source, a webcam

4.  [DONE] **SELECT** **RECORD** **STOP** **RECORD AGAIN** : recording callback works well this way.

5.  [DONE] **PLAYBACK** **STOP** **PLAYBACK AGAIN**: Playback the data coming from one of the available sources

6.  [DONE] **RECORD** : Record the data coming from any of the available sources.

7.  [DONE] **RECORD** AND **HEAR** the recorded sound WORK ! Current working case: record sound with webcam, hear it from the loudspeakers

8.  [DONE] understand how it works AND implement a ring buffer (means be Windows ready on Linux).

9.  [DONE] **FORWARD** : be able to send the recorded sound to a given stream

10. [DONE] **RECORD TO DISK** : be able to record a file containing audio (audiofile.wav currently)

11. [DONE] **RESET** : be able to reset/update the list

12. [DONE] Figure out how to synchronize audio and vidéo.

13. [DONE] mux audio + video (based on ffmpeg API)

14. [DONE] directly create any video of any type between .avi, .mkv, .mp4, .mov and .flv 

15. [TODO] Allow to create .webm (currently broken for some reason)

15. [DONE] optimize the muxer (mostly synchronization issues between audio + video)

16. [WIP] factorize and implement the code into miniDart

16. [TODO] Improve sound quality (including volume level)

17. [TODO] shortcut obsolete opencv wrapper, and use ffmpeg streams directly

