/* 
 * file canvas.cpp, belongs to miniDart project
 * Copyright Eric Bachard 2019 August 19th   17:55:00   CEST
 * License : GPL v2
 * See: http://www.gnu.org/licenses/gpl-2.0.html
 */

#include "canvas.hpp"
#include "helpers.h"
#include "imgui_helpers.h"

#ifdef BUILDING_FRENCH
#include "miniDart_fr.hpp"
#else
#include "miniDart_en-US.hpp"
#endif

#define MINIMAL_AREA_SIZE 4.0f

enum ObjActions
{
    PUT_OBJECT_ON_TOP          = 0,
    ONE_LEVEL_UP               = 1,
    ONE_LEVEL_DOWN             = 2,
    PUT_OBJECT_AT_THE_BOTTOM   = 3,
    DELETE_OBJECT              = 4
};

template<typename T> static inline T ImMin(T lhs, T rhs)   { return lhs < rhs ? lhs : rhs; }
template<typename T> static inline T ImMax(T lhs, T rhs)   { return lhs >= rhs ? lhs : rhs; }


const std::string canvasObjectImagePath[CANVAS_OBJECTS_TYPES_MAX] =
{
    SELECT_CURSOR_IMAGE_DARK_PATH,
    TEXT_OBJECT_IMAGE_DARK_PATH,
    ANGLE_MEASURE_IMAGE_DARK_PATH,
    FILLED_RECTANGLE_IMAGE_DARK_PATH,
    EMPTY_RECTANGLE_IMAGE_DARK_PATH,
    FILLED_CIRCLE_IMAGE_DARK_PATH,
    EMPTY_CIRCLE_IMAGE_DARK_PATH,
    FILLED_ELLIPSE_IMAGE_DARK_PATH,
    EMPTY_ELLIPSE_IMAGE_DARK_PATH,
    RANDOM_LINE_IMAGE_DARK_PATH,
    RANDOM_ARROW_IMAGE_DARK_PATH,
    SIMPLE_ARROW_IMAGE_DARK_PATH,
    SIMPLE_LINE_IMAGE_DARK_PATH,
};



md::Canvas::Canvas()
{
};

md::Canvas::~Canvas()
{
};


void md::Canvas::loadCanvasObjectsIcons(void)
{
    for (short i = 0 ; i < CANVAS_OBJECTS_TYPES_MAX ; i++)
    {
        canvasObjectImage[i] = cv::imread(canvasObjectImagePath[i]);
    }
}

void md::Canvas::createCanvasObjectsImagesTexIds(void)
{
    for (short int i = 0; i < CANVAS_OBJECTS_TYPES_MAX ; i++)
    {
        this->canvasObjectImageTexId[i] = glConvertMatToTexture(canvasObjectImage[i]);
    }
}

void md::Canvas::cleanCanvasObjectsImagesTexIds(void)
{
    short int i = 0;
    for (i = 0; i < CANVAS_OBJECTS_TYPES_MAX ; i++)
    {
        if (canvasObjectImageTexId[i] != 0)
            glDeleteTextures(1, &canvasObjectImageTexId[i]);
    }
}

bool md::Canvas::init()
{
    adding_rect     = false;
    adding_preview1 = false;
    adding_rect2    = false;
    adding_preview2 = false;

    topLeft = ImVec2(0.0f, 0.0f);
    bottomRight = ImVec2(0.0f, 0.0f);

    DrawnObject aDrawnObject;
    aDrawnObject.thickness = DEFAULT_OBJECT_THICKNESS;

    // explicitely unselect the next to be drawn object
    aDrawnObject.selected = false;

    setObjectCurrentlySelected(false);

    // the first selected object has index -1
    currentActiveDrawnObjectIndex = -1;

    p_currentlyDrawnObjects = &currentlyDrawnObjects;

    iconWidth     = DEFAULT_ICON_WIDTH;
    iconHeight    = DEFAULT_ICON_HEIGHT;
    frame_padding = DEFAULT_FRAME_PADDING;
    bcol          = ImVec4(0.3f, 0.4f, 1.0f, 0.5f);
    // ocol       = ImVec4(0.4f, 0.4f, 0.4f, 0.5f);
    return true;
}


void md::Canvas::preview(int selectedObject, ImU32 color, int w, float ratio, float outline_thickness)
{
    setMousePosValid(w, ratio);

    if ((FILLED_ELLIPSE == selectedObject) || (EMPTY_ELLIPSE == selectedObject))
    {
        aDrawnObject.radius_x = 1.0f + ImGui::GetMouseDragDelta().x;
        aDrawnObject.radius_y = 1.0f + ImGui::GetMouseDragDelta().y;

        if (fabs(aDrawnObject.radius_x) <= 1.0f)
            aDrawnObject.radius_x = 1.0f;

        aDrawnObject.rotation = ImGui::GetIO().KeyCtrl ? aDrawnObject.radius_y / aDrawnObject.radius_x : 0.0f;
    }

    switch(aDrawnObject.anObjectType)
    {
        // pour TOUS les objets SAUF le segment de droite OU la flèche,
        // on utilise la méthode catchPrimitivePoints()
        // pour prévisualiser l'objet à dessiner
        case SELECTED_OBJECT:
        case EMPTY_RECTANGLE:
        case EMPTY_CIRCLE:
        case EMPTY_ELLIPSE:
        case FILLED_RECTANGLE:
        case FILLED_CIRCLE:
        case FILLED_ELLIPSE:
        case SIMPLE_LINE:
        case SIMPLE_ARROW:
        {
            catchPrimitivesPoints();
        }
        break;

        case RANDOM_LINE:
            // static is IMPORTANT, and b_drawing cannot be set as true at first pass.
            // Without being static, adding circle would always be false
            static bool b_drawing   = false;

            // si l'objet à dessiner est une ligne quelconque
            // et que l'on est en train de dessiner
            if (b_drawing)
            {
                // on stocke le type d'objet (celui qui est actif) dans l'instance de l'objet en train d'être dessiné
                aDrawnObject.anObjectType = selectedObject;

                // on stocke le premier point
                aDrawnObject.objectPoints.push_back(mouse_pos_in_image);

                // pré-visualisation de l'objet dessiné en utilisant les informations temporaires
                // (il n'est pas encore définitivement stocké dans le vecteur d'objets)
                for (int i = 0 ; i < aDrawnObject.objectPoints.size(); i++)
                {
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[i].x,
#else
                    ImGui::GetForegroundDrawList()->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[i].x,
#endif
                                                                        mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[i].y),
                                                                 aDrawnObject.thickness,
                                                                 aDrawnObject.objBackgroundColor,
                                                                 8);
                }

                // on était en train de dessiner (b_drawing == true), et on a relâché la souris (!ImGui::GetIO().MouseDown[0])
                if (!ImGui::GetIO().MouseDown[0])
                {
                    // on n'est plus en train de dessiner cet objet, car on a relâché le bouton de la souris
                    b_drawing = false;

                    // l'objet actuel n'étant PAS ACTIF, on peut stocker l'objet temporaire appelé "aDrawnObject" dans la pile des objets 
                    // (ses coordonnées sont complètes et déjà incluses)
                    if (getIsAnObjectActive() == false)
                        p_currentlyDrawnObjects->push_back(aDrawnObject);

                    // maintenant qu'on a stocké l'objet dans le vecteur des objets à dessiner,
                    // on vide la pile temporaire des informations contenues dans aDrawnObject
                    while (!aDrawnObject.objectPoints.empty())
                    {
                        aDrawnObject.objectPoints.pop_back();
                    }
                    // l'objet temporaire aDrawnObject est maintenant vide
                }
            }
            // le curseur de la souris est à l'intérieur du cadre dans lequel on peut dessiner
            if (ImGui::IsItemHovered())
            {
                // on survole le cadre, avec un des boutons DÉJÀ appuyé MAIS on n'est pas en train de déplacer le curseur de la souris
                if ( (ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1)) && !ImGui::IsMouseDragging(0) )
                {
                    // on ne dessine plus
                    b_drawing = false;
                }
 
                // on survole le cadre ET on n'a pas relâché le bouton de la souris ET on déplace le curseur de la souris : on dessine
                if ( (ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1)) && ImGui::IsMouseDragging(0) )
                    b_drawing = true;

                // on n'est PLUS en train de dessiner, le curseur de la souris s'est arrêté, mais on appuie toujours sur le 1er bouton
                if ( (!b_drawing && ImGui::IsMouseClicked(0)) )
                {
                    // on ajoute les coordonnées ACTUELLES de la souris dans l'objet en train d'être visualisé
                    // (mais qui n'est pas encore stocké dans la pile)
                    if (getIsAnObjectActive() == false)
                        aDrawnObject.objectPoints.push_back(mouse_pos_in_image);

                    // on est toujours en train de dessiner en fait, et il faut réactiver "dessin en cours"
                    b_drawing = true;
                }
            }
        break;

        case RANDOM_ARROW:
            static bool b_drawing2   = false;

            if (b_drawing2)
            {
                aDrawnObject.anObjectType = selectedObject;
                arrow_points.push_back(mouse_pos_in_image);

                for (int i = 0 ; i < arrow_points.size(); i++)
                {
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddCircleFilled( ImVec2(mp_TextCanvas->image_pos.x + arrow_points[i].x, mp_TextCanvas->image_pos.y + arrow_points[i].y), aDrawnObject.thickness, aDrawnObject.objBackgroundColor, 8);
#else
                    ImGui::GetForegroundDrawList()->AddCircleFilled( ImVec2(mp_TextCanvas->image_pos.x + arrow_points[i].x, mp_TextCanvas->image_pos.y + arrow_points[i].y), aDrawnObject.thickness, aDrawnObject.objBackgroundColor, 8);
#endif
                }

                if (!ImGui::GetIO().MouseDown[0])
                {
                    b_drawing2 = false;

                    aDrawnObject.objectPoints.push_back(arrow_points[0]);
                    aDrawnObject.objectPoints.push_back(arrow_points  [(int)(arrow_points.size()/3.0f)]);
                    aDrawnObject.objectPoints.push_back(arrow_points[(int)((2*arrow_points.size())/3.0f)]);
                    aDrawnObject.objectPoints.push_back(arrow_points[arrow_points.size()-1]);
                    aDrawnObject.P1P4 = sqrtf(  (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                               *(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                              + (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y)
                                               *(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y));

                    if (getIsAnObjectActive() == false)
                        p_currentlyDrawnObjects->push_back(aDrawnObject);

                    arrow_points.clear();
                    aDrawnObject.objectPoints.clear();
                }
            }

            // Item = le cadre qui contient le dessin
            if (ImGui::IsItemHovered())
            {
                // on n'a pas relâché le bouton de la souris, mais on a arrêté de déplacer le curseur de la souris
                if ( (ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1)) && !ImGui::IsMouseDragging(0) )
                    b_drawing2 = false;

                // on n'a pas relaché la souris, et on déplace le curseur
                if ( (ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1)) && ImGui::IsMouseDragging(0) )
                    b_drawing2 = true;

                // on n'a pas relâché le 1er bouton de la souris, mais on a arrêté de déplacer le curseur
                // => on peut stocker la seconde coordonnées de la flèche (seulement si l'objet concerné n'est PAS ACTIF
                if ( (!b_drawing2 && ImGui::IsMouseClicked(0)) )
                {
                    if (getIsAnObjectActive() == false)
                        arrow_points.push_back(mouse_pos_in_image);

                    // comme on a seulement arreêté de déplacer le curseur de la souris,
                    // on est TOUJOURS en train de dessiner et il faut activer à nouveau l'état "en train de dessiner"
                    b_drawing2 = true;
                }
            }
        break;

        //case TEXT_OBJECT:
        case SELECT_CURSOR:
        case NOT_A_DRAWN_OBJECT:
        {
            if (adding_rect)
            {
                adding_preview1 = true;
                zoom_area_points.push_back(mouse_pos_in_image); // catch the second point

                if (!ImGui::GetIO().MouseDown[0])
                   adding_rect = adding_preview1 = false;
            }


            if (ImGui::IsItemHovered())
            {

                if ( (((ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1) )  && (!zoom_area_points.empty()))) && !ImGui::IsMouseDragging(0) )
                {
                    adding_rect = false;
                    adding_preview1 = false;
                    zoom_area_points.pop_back();
                    zoom_area_points.pop_back();
                }

                if ( (!adding_rect && ImGui::IsMouseClicked(0)) )
                {
                    zoom_area_points.push_back(mouse_pos_in_image);
                    adding_rect = true;
                }
            }

            updateSelectedArea(zoom_area_points, color, outline_thickness);
            reorder_points(&topLeft, &bottomRight);

            if (adding_preview1)
                zoom_area_points.pop_back();
        }
        break;

        case TEXT_OBJECT:
        default:
        break;
    }
}

void md::Canvas::updateSelectedArea(ImVector <ImVec2> zoom_area_points, ImU32 color, float outline_thickness)
{
    for (int i = 0; i < zoom_area_points.Size - 1; i += 2)
    {
        if ((abs(zoom_area_points[i].x - zoom_area_points[i+1].x) > 2) && (abs(zoom_area_points[i].y - zoom_area_points[i+1].y)> 2))
        {
#if (IMGUI_VERSION_NUM <= 17906)
            ImGui::GetOverlayDrawList()->AddRect(ImVec2(mp_TextCanvas->image_pos.x + zoom_area_points[i].x, mp_TextCanvas->image_pos.y + zoom_area_points[i].y),
#else
            ImGui::GetForegroundDrawList()->AddRect(ImVec2(mp_TextCanvas->image_pos.x + zoom_area_points[i].x, mp_TextCanvas->image_pos.y + zoom_area_points[i].y),
#endif
                                                 ImVec2(mp_TextCanvas->image_pos.x + zoom_area_points[i+1].x, mp_TextCanvas->image_pos.y + zoom_area_points[i+1].y),
                                                 color, 0.0f, ~0 ,outline_thickness);
            topLeft = zoom_area_points[i];
            bottomRight = zoom_area_points[i+1];
        }
        else
        {
            topLeft = ImVec2(0.0f,0.0f);
            bottomRight = ImVec2(ZOOM_WIDTH_MIN, ZOOM_HEIGHT_MIN);
        }
    }
}


void md::Canvas::update(ImVec2 mousePos)
{
    static bool b_is_popup_open = false;

    for (unsigned int i = 0; i < p_currentlyDrawnObjects->size(); i++)
    {
        switch(currentlyDrawnObjects[i].anObjectType)
        {
            // never select RANDOM_ARROW to allow superposition with other objects
            case RANDOM_ARROW:
                p_currentlyDrawnObjects->at(i).hovered = false;
            break;

            case FILLED_RECTANGLE:
                //currentlyDrawnObjects[i].hovered = insideFilledRectangle(mousePos, currentlyDrawnObjects[i].objectPoints);
                p_currentlyDrawnObjects->at(i).hovered = insideFilledRectangle(mousePos, p_currentlyDrawnObjects->at(i).objectPoints);
            break;

            case FILLED_CIRCLE:
                p_currentlyDrawnObjects->at(i).hovered = insideCircle(mousePos, p_currentlyDrawnObjects->at(i).objectPoints[0], p_currentlyDrawnObjects->at(i).R2_out );
            break;

            case FILLED_ELLIPSE:
                p_currentlyDrawnObjects->at(i).hovered = insideEllipse(mousePos,
                                                                 p_currentlyDrawnObjects->at(i).long_axis,
                                                                 p_currentlyDrawnObjects->at(i).F1,
                                                                 p_currentlyDrawnObjects->at(i).F2);
            break;

            case EMPTY_RECTANGLE:
                //WORKS OK TOO
                //currentlyDrawnObjects[i].hovered =
                        //insidePolygon(mousePos, currentlyDrawnObjects[i].Rect_ext) && !insidePolygon(mousePos, currentlyDrawnObjects[i].Rect_int);
                p_currentlyDrawnObjects->at(i).hovered =
                        intersectEmptyRectangle(mousePos, p_currentlyDrawnObjects->at(i).Rect_ext, p_currentlyDrawnObjects->at(i).Rect_int );
            break;

            case EMPTY_CIRCLE:
                p_currentlyDrawnObjects->at(i).hovered = intersectEmptyCircle(mousePos,
                                                                            p_currentlyDrawnObjects->at(i).objectPoints[0],
                                                                            p_currentlyDrawnObjects->at(i).R2_in,
                                                                            p_currentlyDrawnObjects->at(i).R2_out);
            break;

            case EMPTY_ELLIPSE:
                p_currentlyDrawnObjects->at(i).hovered = intersectEmptyEllipse(mousePos,
                                                                         p_currentlyDrawnObjects->at(i).long_axis,
                                                                         p_currentlyDrawnObjects->at(i).F1,
                                                                         p_currentlyDrawnObjects->at(i).F2,
                                                                         p_currentlyDrawnObjects->at(i).thickness);
            break;

            case SIMPLE_ARROW:
                p_currentlyDrawnObjects->at(i).hovered = insideSimpleArrow(mousePos, p_currentlyDrawnObjects->at(i).objectPoints, p_currentlyDrawnObjects->at(i).arrowPolygon);
            break;

            case SIMPLE_LINE:
                p_currentlyDrawnObjects->at(i).hovered = intersectSegment(mousePos, p_currentlyDrawnObjects->at(i).objectPoints[0], p_currentlyDrawnObjects->at(i).objectPoints[1]);
            break;

            default:
            break;
        }

        // hack
        static int a_count = 0;

        if (a_count > 60)
        {
            if ((false == getIsAnObjectActive()) || (p_currentlyDrawnObjects->size() == 0))
            {
                //std::cout << " b_is_popup_open changing to false" << "\n";
                b_is_popup_open = false;
            }

            a_count = 0;
        }
        else
            a_count++;

        // un objet est ACTIF ET on le déplace ET il n'y a pas de popup ouvert
        if ((ImGui::IsMouseDragging(0)) && (p_currentlyDrawnObjects->at(i).selected == true) && (!b_is_popup_open))
        {

            // on modifie les composantes des 2 POINTS PRINCIPAUX de cet objet (communs à tous les objets dessinés)
            // en tenant compte du déplacement
            p_currentlyDrawnObjects->at(i).objectPoints[0].x += ImGui::GetIO().MouseDelta.x;
            p_currentlyDrawnObjects->at(i).objectPoints[0].y += ImGui::GetIO().MouseDelta.y;
            p_currentlyDrawnObjects->at(i).objectPoints[1].x += ImGui::GetIO().MouseDelta.x;
            p_currentlyDrawnObjects->at(i).objectPoints[1].y += ImGui::GetIO().MouseDelta.y;

            switch(p_currentlyDrawnObjects->at(i).anObjectType)
            {
                case EMPTY_RECTANGLE:
                    // FIXME : should be :
                    //p_currentlyDrawnObjects->at(i).Rect_ext[0] += ImGui::GetIO().MouseDelta;
                    p_currentlyDrawnObjects->at(i).Rect_ext[0].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_ext[0].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_ext[1].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_ext[1].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_ext[2].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_ext[2].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_ext[3].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_ext[3].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_int[0].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_int[0].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_int[1].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_int[1].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_int[2].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_int[2].y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).Rect_int[3].x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).Rect_int[3].y += ImGui::GetIO().MouseDelta.y;
                break;

                case FILLED_ELLIPSE:
                case EMPTY_ELLIPSE:
                    p_currentlyDrawnObjects->at(i).F1.x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).F1.y += ImGui::GetIO().MouseDelta.y;
                    p_currentlyDrawnObjects->at(i).F2.x += ImGui::GetIO().MouseDelta.x;
                    p_currentlyDrawnObjects->at(i).F2.y += ImGui::GetIO().MouseDelta.y;
                break;

                default:
                break;
            }
        }

        //    OBJET DEVIENT ACTIF : ssi il est survolé ET on clique sur le bouton de la souris ET on n'est pas en train de faire glisser l'objet
        //    AUCUN OBJET ACTIF i.e. TOUT OBJET DEVIENT INACTIF : ssi
        //        - il est survolé ET s'il est ACTIF ET si on clique dessus ET on ne le modifie pas (on n'a pas ouvert le popup)
        //        - on clique en dehors de tout objet ET on clique sur le bouton de la souris ET on ne le modifie pas (on n'a pas ouvert le popup)
        if (p_currentlyDrawnObjects->at(i).hovered == true)
        {
            if ((ImGui::IsMouseClicked(0)) && !ImGui::IsMouseDragging(0))
            {
                b_is_popup_open = false;

                if (true == p_currentlyDrawnObjects->at(i).selected)
                {
//#define IMPROVE_CANVAS
#ifdef IMPROVE_CANVAS
                    unselectObject(i);
                    std::cout << "b_is_popup_open vaut : " << b_is_popup_open << "\n";
                    std::cout << "Object unselected."                   << "\n";
                    std::cout << "Current  index of object = " << i << "\n";
                    std::cout << "Actualy, object selected status is : " << getIsAnObjectActive() << "\n";
                    std::cout << "Returned index of current selected object = " << (int)getCurrentActiveDrawnObjectIndex() << "\n";

#endif
                }
                else
                {
#ifdef IMPROVE_CANVAS
                    std::cout << "b_is_popup_open vaut : " << b_is_popup_open << "\n";
#endif
                    unselectAllObjects(p_currentlyDrawnObjects->size());
                    p_currentlyDrawnObjects->at(i).selected = true;
                    setSelected(i);
                    setObjectCurrentlySelected(true);
#ifdef IMPROVE_CANVAS
                    std::cout << "Object " << i << " has been selected.         " << "\n";
                    std::cout << "Actualy, object selected status is :          " << getIsAnObjectActive() << "\n";
                    std::cout << "Returned index of currently selected object = " << (int)getCurrentActiveDrawnObjectIndex() << "\n";
#endif
                }
#ifdef IMPROVE_CANVAS
                std::cout << " Object " << i << " clicked and status is " << p_currentlyDrawnObjects->at(i).selected << "\n";
#endif
            }
        }


    }

    // UN OBJET est ACTIF
    //                          ET le dialogue de modification de l'objet n'est PAS ouvert
    //                          ET on clique sur le bouton de la souris
    //                          ET on n'est pas en train de déplacer l'objet
    //                          ET l'objet ACTIF n'est PAS survolé
    if ((getIsAnObjectActive()) && (!b_is_popup_open))
    {
        if  (   (false == p_currentlyDrawnObjects->at(getCurrentActiveDrawnObjectIndex()).hovered)
             && (ImGui::IsMouseClicked(0))
             && (!ImGui::IsMouseDragging(0))
             && (!b_is_popup_open)
            )
        {
            unselectAllObjects(p_currentlyDrawnObjects->size());
        }
    }

    // Il faut avoir dessiné au moins 1 objet pour que ouvrir ce popup ait un sens
    if (p_currentlyDrawnObjects->size() > 0)
    {
        // il faut aussi qu'un objet soit actif pour continuer
        if (true == getIsAnObjectActive())
        {
            ImGui::OpenPopupOnItemClick("ActiveObjectPopup");

            if (ImGui::BeginPopupModal("ActiveObjectPopup" , NULL, ImGuiWindowFlags_AlwaysAutoResize))
            {
                b_is_popup_open = true;

                static bool action_done = false;
                static int action = -1;
                static ImU32 objColor = currentlyDrawnObjects[getCurrentActiveDrawnObjectIndex()].objBackgroundColor;
                static ImVec4 col  = ImColor(objColor);

                ImGui::SetItemDefaultFocus();

                if (ImGui::Selectable(PUT_ON_TOP))
                    action = PUT_OBJECT_ON_TOP;

                if (ImGui::Selectable(PUT_ONE_LEVEL_UP))
                    action = ONE_LEVEL_UP;

                if (ImGui::Selectable(PUT_ONE_LEVEL_DOWN))
                    action = ONE_LEVEL_DOWN;

                if (ImGui::Selectable(PUT_AT_THE_BOTTOM))
                    action = PUT_OBJECT_AT_THE_BOTTOM;

                if (ImGui::Selectable(DELETE_THE_DRAWN_OBJECT))
                     action = DELETE_OBJECT;

                ImGui::ColorEdit4("   " , &col.x);

                static int aSelectedObject = (int)getCurrentActiveDrawnObjectIndex();

                if (action > -1)
                {
                    action_done = moveObjectTo(getCurrentActiveDrawnObjectIndex(), action);

                    if (false == action_done)
                        std::cerr << "Pb with moveObjectTO" << "\n";

                    if (p_currentlyDrawnObjects->size() == 0)
                    {
                        setObjectCurrentlySelected(false);
                        setSelected(-1);
                    }

                    unselectObject(aSelectedObject);
                    action = -1;
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::Button(CANCEL_BUTTON))
                {
                    unselectObject(aSelectedObject);
                    action = -1;
                    ImGui::CloseCurrentPopup();
                }
                ImGui::SameLine();

                if (ImGui::Button(CHOOSE_THIS_COLOR))
                {
                    currentlyDrawnObjects[getCurrentActiveDrawnObjectIndex()].objBackgroundColor =
                                            IM_COL32((int)(col.x * 255), (int)(col.y * 255), (int)(col.z * 255), (int)(col.w * 255));

                    std::cout << "objColor : " << (int)objColor << "\n";

                    unselectObject(aSelectedObject);
                    action = -1;
                    ImGui::CloseCurrentPopup();
                }
                ImGui::EndPopup();
            } // End BeginPopupModal()
        }
    }
}

bool  md::Canvas::addObject()
{
    return true;
}



void  md::Canvas::setMousePosValid(int w, float ratio)
{
    mouse_pos_in_image = ImVec2(ImGui::GetIO().MousePos.x - mp_TextCanvas->image_pos.x,  (ImGui::GetIO().MousePos.y - mp_TextCanvas->image_pos.y));

    if ( mouse_pos_in_image.x < 0 )
        mouse_pos_in_image.x = 0.0f;//LEFT_IMAGE_BORDER;

    if (( mouse_pos_in_image.y < 0 ))
        mouse_pos_in_image.y = 0.0f;//TOP_IMAGE_BORDER;

    if ( mouse_pos_in_image.x > (RIGHT_IMAGE_BORDER) )
        mouse_pos_in_image.x = RIGHT_IMAGE_BORDER;

    if (( mouse_pos_in_image.y > BOTTOM_IMAGE_BORDER ))
        mouse_pos_in_image.y = BOTTOM_IMAGE_BORDER;
}


ImU32 md::Canvas::getBackgroundColor(unsigned int i)
{
    ImU32 toReturn = p_currentlyDrawnObjects->at(i).objBackgroundColor;

    if (p_currentlyDrawnObjects->at(i).hovered == true)
            toReturn = IM_COL32(128, 128, 128, 128);//IM_COL32_WHITE;

    if (getCurrentActiveDrawnObjectIndex() == i)
            toReturn = IM_COL32(192, 192, 192, 192);//IM_COL32_WHITE;

    return toReturn;
}


void  md::Canvas::unselectObject(int anObject)
{
    if ((int)p_currentlyDrawnObjects->size() > anObject)
    {
        p_currentlyDrawnObjects->at(anObject).selected = false;
        setSelected(-1);
        setObjectCurrentlySelected (false);
    }
}

void  md::Canvas::unselectAllObjects(int aSize)
{
    for (int i = 0 ; i < aSize ; ++i)
    {
        p_currentlyDrawnObjects->at(i).selected = false;
        setSelected(-1);
    }
    setObjectCurrentlySelected (false);
}


void md::Canvas::catchPrimitivesPoints(void)
{
    if (adding_rect2)
    {
        adding_preview2 = true;
        aDrawnObject.objectPoints.push_back(mouse_pos_in_image);

        aDrawnObject.P1P4 = sqrtf(  (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                  + (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y)*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y) );
        // preview
        if (!aDrawnObject.objectPoints.empty())
        {
            switch(aDrawnObject.anObjectType)
            {
                case EMPTY_RECTANGLE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddRect(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddRect(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                         ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[1].x,
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[1].y),
                                                         aDrawnObject.objBackgroundColor,
                                                         0.0f,
                                                         ~0 ,
                                                         aDrawnObject.thickness);
                break;

                case EMPTY_ELLIPSE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddEllipse(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddEllipse(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                   mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                            aDrawnObject.radius_x,
                                                            aDrawnObject.radius_y,
                                                            aDrawnObject.objBackgroundColor,
                                                            aDrawnObject.rotation,
                                                            32,
                                                            aDrawnObject.thickness);
                break;

                case FILLED_ELLIPSE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddEllipseFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddEllipseFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                         mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                                  aDrawnObject.radius_x,
                                                                  aDrawnObject.radius_y,
                                                                  aDrawnObject.objBackgroundColor,
                                                                  aDrawnObject.rotation,
                                                                  32);
                break;

                case EMPTY_CIRCLE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddCircle(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddCircle(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                  mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                           aDrawnObject.P1P4,
                                                           aDrawnObject.objBackgroundColor, 32, aDrawnObject.thickness);
                break;

                case FILLED_RECTANGLE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddRectFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddRectFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                      mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                               ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[1].x,
                                                                      mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[1].y),
                                                               aDrawnObject.objBackgroundColor);
                break;

                case FILLED_CIRCLE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                        mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                                 aDrawnObject.P1P4,
                                                                 aDrawnObject.objBackgroundColor,
                                                                 32);
                break;

                case SIMPLE_LINE:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddLine(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddLine(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                         ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[1].x,
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[1].y),
                                                         aDrawnObject.objBackgroundColor,
                                                         aDrawnObject.thickness);
                break;

                case SIMPLE_ARROW:
#if (IMGUI_VERSION_NUM <= 17906)
                    ImGui::GetOverlayDrawList()->AddLine(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#else
                    ImGui::GetForegroundDrawList()->AddLine(ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[0].x,
#endif
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[0].y),
                                                         ImVec2(mp_TextCanvas->image_pos.x + aDrawnObject.objectPoints[1].x,
                                                                mp_TextCanvas->image_pos.y + aDrawnObject.objectPoints[1].y),
                                                         aDrawnObject.objBackgroundColor,
                                                         aDrawnObject.thickness);

                    if (aDrawnObject.P1P4 > 1.5f * aDrawnObject.arrowLength)
                    {
                         ImVec2 pointC(  aDrawnObject.objectPoints[1].x - (aDrawnObject.arrowLength * (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4,
                                         aDrawnObject.objectPoints[1].y - (aDrawnObject.arrowLength * (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4);
                         ImVec2 pointD(  pointC.x + (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4,
                                         pointC.y - (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4);
                         ImVec2 pointE(  pointC.x - (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4,
                                         pointC.y + (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4);

#if ( IMGUI_VERSION_NUM <= 17906)
                         ImGui::GetOverlayDrawList()->PathClear();
                         ImGui::GetOverlayDrawList()->PathLineTo(ImVec2(pointD.x + mp_TextCanvas->image_pos.x, pointD.y + mp_TextCanvas->image_pos.y));
                         ImGui::GetOverlayDrawList()->PathLineTo(ImVec2(aDrawnObject.objectPoints[1].x + mp_TextCanvas->image_pos.x, aDrawnObject.objectPoints[1].y + mp_TextCanvas->image_pos.y));
                         ImGui::GetOverlayDrawList()->PathLineTo(ImVec2(pointE.x + mp_TextCanvas->image_pos.x, pointE.y + mp_TextCanvas->image_pos.y));
                         ImGui::GetOverlayDrawList()->PathStroke(aDrawnObject.objBackgroundColor, false, aDrawnObject.thickness);
#else
                         ImGui::GetForegroundDrawList()->PathClear();
                         ImGui::GetForegroundDrawList()->PathLineTo(ImVec2(pointD.x + mp_TextCanvas->image_pos.x, pointD.y + mp_TextCanvas->image_pos.y));
                         ImGui::GetForegroundDrawList()->PathLineTo(ImVec2(aDrawnObject.objectPoints[1].x + mp_TextCanvas->image_pos.x, aDrawnObject.objectPoints[1].y + mp_TextCanvas->image_pos.y));
                         ImGui::GetForegroundDrawList()->PathLineTo(ImVec2(pointE.x + mp_TextCanvas->image_pos.x, pointE.y + mp_TextCanvas->image_pos.y));
                         ImGui::GetForegroundDrawList()->PathStroke(aDrawnObject.objBackgroundColor, false, aDrawnObject.thickness);
#endif
                    }
                break;

                default:
                break;
            }
        } // end preview

        if (!ImGui::GetIO().MouseDown[0])
        {
#ifdef DEBUG
            std::cout << "Souris relâchée !!  " << __LINE__ << std::endl;
#endif
            adding_rect2    = false;
            adding_preview2 = false;

            switch(aDrawnObject.anObjectType)
            {
                case FILLED_ELLIPSE:
                case EMPTY_ELLIPSE:
                {
                    float radius_x = aDrawnObject.radius_x;
                    float radius_y = aDrawnObject.radius_y;

                    if (radius_x < 0)
                        radius_x = -radius_x;

                    if (radius_y < 0)
                        radius_y = -radius_y;

                    aDrawnObject.long_axis = ImMax(radius_x, radius_y);

                    float short_axis = ImMin(radius_x, radius_y);
                    float e = sqrtf (1 - ((short_axis * short_axis )/(aDrawnObject.long_axis * aDrawnObject.long_axis)) );
                    float xF = e * aDrawnObject.long_axis * cos(M_PI * aDrawnObject.rotation / 180.0f);
                    float yF = e * aDrawnObject.long_axis * sin(M_PI * aDrawnObject.rotation / 180.0f);

                    aDrawnObject.F1.x = aDrawnObject.objectPoints[0].x;
                    aDrawnObject.F1.y = aDrawnObject.objectPoints[0].y;
                    aDrawnObject.F2.x = aDrawnObject.F1.x;
                    aDrawnObject.F2.y = aDrawnObject.F1.y;

                    if (radius_x > radius_y)
                    {
                        aDrawnObject.F1.x += xF;
                        aDrawnObject.F1.y += yF;
                        aDrawnObject.F2.x -= xF;
                        aDrawnObject.F2.y -= yF;
                    }

                    if (radius_x < radius_y)
                    {
                        aDrawnObject.F1.x += yF;
                        aDrawnObject.F1.y += xF;
                        aDrawnObject.F2.x -= yF;
                        aDrawnObject.F2.y -= xF;
                    }

//#define DEBUG_ELLIPSES
#ifdef DEBUG_ELLIPSES
                    std::cout << "xF         = " << xF << "\n";
                    std::cout << "yF         = " << yF << "\n";
                    std::cout << "e          = " << e << "\n";
                    std::cout << "long_axis  = " << aDrawnObject.long_axis << "\n";
                    std::cout << "short_axis = " << short_axis << "\n";


                    std::cout << "aDrawnObject.rotation : " << aDrawnObject.rotation << "\n";
                    std::cout << "aDrawnObject.objectPoints[0].x : " << aDrawnObject.objectPoints[0].x << "\n";
                    std::cout << "aDrawnObject.objectPoints[0].y : " << aDrawnObject.objectPoints[0].y << "\n";

                    std::cout << "aDrawnObject.F1.x : " << aDrawnObject.F1.x << "\n";
                    std::cout << "aDrawnObject.F1.y : " << aDrawnObject.F1.y << "\n";
                    std::cout << "aDrawnObject.F2.x : " << aDrawnObject.F2.x << "\n";
                    std::cout << "aDrawnObject.F2.y : " << aDrawnObject.F2.y << "\n";
#endif
                }
                break;

                case EMPTY_CIRCLE:
                {
                    aDrawnObject.P1P4 = sqrtf(  (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                              + (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y)*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y) );

                    float Rext = aDrawnObject.P1P4 + (aDrawnObject.thickness / 2.0f);
                    float Rint = aDrawnObject.P1P4 - (aDrawnObject.thickness / 2.0f);
                    aDrawnObject.R2_in = Rint * Rint;
                    aDrawnObject.R2_out = Rext * Rext;
                }
                break;

                case FILLED_CIRCLE:
                {
                    aDrawnObject.R2_out = (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                              + (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y)*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y);
                    aDrawnObject.P1P4 = sqrtf(aDrawnObject.R2_out);
                }
                break;

                case EMPTY_RECTANGLE:
                {
                    reorder_points(&aDrawnObject.objectPoints[0], &aDrawnObject.objectPoints[1]);

                    aDrawnObject.Rect_ext.push_back(ImVec2( aDrawnObject.objectPoints[0].x - (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[0].y - (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_ext.push_back(ImVec2( aDrawnObject.objectPoints[1].x + (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[0].y - (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_ext.push_back(ImVec2( aDrawnObject.objectPoints[1].x + (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[1].y + (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_ext.push_back(ImVec2( aDrawnObject.objectPoints[0].x - (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[1].y + (aDrawnObject.thickness /2.0f)));

                    aDrawnObject.Rect_int.push_back(ImVec2( aDrawnObject.objectPoints[0].x + (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[0].y + (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_int.push_back(ImVec2( aDrawnObject.objectPoints[1].x - (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[0].y + (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_int.push_back(ImVec2( aDrawnObject.objectPoints[1].x - (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[1].y - (aDrawnObject.thickness /2.0f)));
                    aDrawnObject.Rect_int.push_back(ImVec2( aDrawnObject.objectPoints[0].x + (aDrawnObject.thickness /2.0f),
                                                            aDrawnObject.objectPoints[1].y - (aDrawnObject.thickness /2.0f)));
                }
                break;

                case SIMPLE_ARROW:
                {

                    aDrawnObject.arrowPolygon.push_back(ImVec2(  aDrawnObject.objectPoints[1].x - (aDrawnObject.arrowLength * (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4,
                                         aDrawnObject.objectPoints[1].y - (aDrawnObject.arrowLength * (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4));

                    aDrawnObject.arrowPolygon.push_back(ImVec2(  aDrawnObject.arrowPolygon[0].x + (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4,
                                         aDrawnObject.arrowPolygon[0].y - (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4));

                    aDrawnObject.arrowPolygon.push_back(ImVec2(  aDrawnObject.arrowPolygon[0].x - (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y))/aDrawnObject.P1P4,
                                         aDrawnObject.arrowPolygon[0].y + (aDrawnObject.arrowWidth*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x))/aDrawnObject.P1P4));

                    aDrawnObject.P1P4 = sqrtf(  (aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)*(aDrawnObject.objectPoints[1].x - aDrawnObject.objectPoints[0].x)
                                              + (aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y)*(aDrawnObject.objectPoints[1].y - aDrawnObject.objectPoints[0].y) );
                }
                break;

                default:
                break;
            }

            // Only add "eye visible" objects
            if ((aDrawnObject.P1P4 >= MINIMAL_AREA_SIZE) && (getIsAnObjectActive() == false))
                 p_currentlyDrawnObjects->push_back(aDrawnObject);

            aDrawnObject.objectPoints.clear();
            aDrawnObject.Rect_ext.clear();
            aDrawnObject.Rect_int.clear();
            aDrawnObject.arrowPolygon.clear();
        }
 
    } /* adding_rect2 */

    if (ImGui::IsItemHovered())
    {
        if ( (ImGui::IsMouseClicked(0)||ImGui::IsMouseClicked(1)) && !ImGui::IsMouseDragging(0) )
        {
            adding_rect2 = adding_preview2 = false;
        }

        if ( (!adding_rect2 && ImGui::IsMouseClicked(0)) && (getIsAnObjectActive() == false))
        {
            aDrawnObject.objectPoints.push_back(mouse_pos_in_image);
            adding_rect2 = true;
        }
    }

    if ((adding_preview2) && !(aDrawnObject.anObjectType == RANDOM_LINE))
        aDrawnObject.objectPoints.pop_back();

} /* Canvas::catchPrimitivesPoints(void) */

 

int md::Canvas::draw()
{
    ImDrawList * p_drawList = ImGui::GetWindowDrawList();

    //mp_TextCanvas->image_size = ImGui::GetContentRegionAvail();

    ImVec2 subview_size = mp_TextCanvas->image_size;

    p_drawList->PushClipRect(ImVec2(0.0f, 0.0f), ImVec2(mp_TextCanvas->image_pos.x + subview_size.x, mp_TextCanvas->image_pos.y + subview_size.y) );

    for (unsigned int i = 0; i < p_currentlyDrawnObjects->size(); i++)
    {
        {
            switch(p_currentlyDrawnObjects->at(i).anObjectType)
            {
                case EMPTY_RECTANGLE:
                    // already stored
                    p_drawList->AddRect(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x, mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                        ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[1].x, mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[1].y),
                                        getBackgroundColor(i),
                                        0.0f,
                                        ~0,
                                        p_currentlyDrawnObjects->at(i).thickness);
                break;

                case EMPTY_CIRCLE:
                                    p_drawList->AddCircle(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                                                 mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                                                 p_currentlyDrawnObjects->at(i).P1P4,
                                                                 getBackgroundColor(i),
                                                                 32, p_currentlyDrawnObjects->at(i).thickness);
                break;

                case FILLED_RECTANGLE:
                    p_drawList->AddRectFilled(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                                     mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                              ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[1].x,
                                                     mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[1].y),
                                              getBackgroundColor(i));
                break;

                case FILLED_CIRCLE:
                    p_drawList->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                                       mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                                p_currentlyDrawnObjects->at(i).P1P4,
                                                getBackgroundColor(i),
                                                32);
                break;

                case EMPTY_ELLIPSE:
                    p_drawList->AddEllipse(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                                  mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                          p_currentlyDrawnObjects->at(i).radius_x,
                                          p_currentlyDrawnObjects->at(i).radius_y,
                                          getBackgroundColor(i),
                                          p_currentlyDrawnObjects->at(i).rotation,
                                          32,
                                          p_currentlyDrawnObjects->at(i).thickness);
                break;

                case FILLED_ELLIPSE:
                    p_drawList->AddEllipseFilled(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                                        mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                                p_currentlyDrawnObjects->at(i).radius_x,
                                                p_currentlyDrawnObjects->at(i).radius_y,
                                                getBackgroundColor(i),
                                                p_currentlyDrawnObjects->at(i).rotation,
                                                32);
                break;

                case SIMPLE_LINE:
                    p_drawList->AddLine(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                               mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                        ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[1].x,
                                               mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[1].y),
                                        getBackgroundColor(i),
                                        p_currentlyDrawnObjects->at(i).thickness);
                break;

                case SIMPLE_ARROW:

                    p_drawList->AddLine(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
                                               mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),
                                        ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[1].x,
                                               mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[1].y),
                                        getBackgroundColor(i),
                                        p_currentlyDrawnObjects->at(i).thickness);

                    if (p_currentlyDrawnObjects->at(i).P1P4 > 1.5f * p_currentlyDrawnObjects->at(i).arrowLength)
                    {
/* FIXME : OPTIMIZE
                        p_drawList->PathClear();
                        p_drawList->PathLineTo(ImVec2(p_currentlyDrawnObjects->at(i).arrowPolygon[1].x + mp_TextCanvas->image_pos.x, p_currentlyDrawnObjects->at(i).arrowPolygon[1].y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(p_currentlyDrawnObjects->at(i).objectPoints[1].x + mp_TextCanvas->image_pos.x, p_currentlyDrawnObjects->at(i).objectPoints[1].y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(p_currentlyDrawnObjects->at(i).arrowPolygon[2].x + mp_TextCanvas->image_pos.x, p_currentlyDrawnObjects->at(i).arrowPolygon[2].y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathStroke(p_currentlyDrawnObjects->at(i).objBackgroundColor, false, p_currentlyDrawnObjects->at(i).thickness);
*/
                        ImVec2 pointC(  p_currentlyDrawnObjects->at(i).objectPoints[1].x - (p_currentlyDrawnObjects->at(i).arrowLength * (p_currentlyDrawnObjects->at(i).objectPoints[1].x - p_currentlyDrawnObjects->at(i).objectPoints[0].x))/p_currentlyDrawnObjects->at(i).P1P4,
                                        p_currentlyDrawnObjects->at(i).objectPoints[1].y - (p_currentlyDrawnObjects->at(i).arrowLength * (p_currentlyDrawnObjects->at(i).objectPoints[1].y - p_currentlyDrawnObjects->at(i).objectPoints[0].y))/p_currentlyDrawnObjects->at(i).P1P4);
                        ImVec2 pointD(  pointC.x + (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[1].y - p_currentlyDrawnObjects->at(i).objectPoints[0].y))/p_currentlyDrawnObjects->at(i).P1P4,
                                        pointC.y - (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[1].x - p_currentlyDrawnObjects->at(i).objectPoints[0].x))/p_currentlyDrawnObjects->at(i).P1P4);
                        ImVec2 pointE(  pointC.x - (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[1].y - p_currentlyDrawnObjects->at(i).objectPoints[0].y))/p_currentlyDrawnObjects->at(i).P1P4,
                                        pointC.y + (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[1].x - p_currentlyDrawnObjects->at(i).objectPoints[0].x))/p_currentlyDrawnObjects->at(i).P1P4);
                        p_drawList->PathClear();
                        p_drawList->PathLineTo(ImVec2(pointD.x + mp_TextCanvas->image_pos.x, pointD.y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(p_currentlyDrawnObjects->at(i).objectPoints[1].x + mp_TextCanvas->image_pos.x, p_currentlyDrawnObjects->at(i).objectPoints[1].y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(pointE.x + mp_TextCanvas->image_pos.x, pointE.y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathStroke(getBackgroundColor(i), false, p_currentlyDrawnObjects->at(i).thickness);
                    }
                break;

                case RANDOM_ARROW:
                    for (int j = 2 ; j < p_currentlyDrawnObjects->at(i).objectPoints.size() ; j++)
                    {
                        //draw a bezier curve
#if (IMGUI_VERSION_NUM <= 18304)
                        p_drawList->AddBezierCurve(   ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
#else
                        p_drawList->AddBezierCubic(   ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[0].x,
#endif
                                                             mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[0].y),  //P1 == start
                                                      ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[1].x,
                                                             mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[1].y), // P2 == CP1
                                                      ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[2].x,
                                                             mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[2].y), // P3 == CP2
                                                      ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[3].x,
                                                             mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[3].y), // P4 == end
                                                      getBackgroundColor(i),
                                                      p_currentlyDrawnObjects->at(i).thickness,
                                                      64); // drawing parameters
                    }

                    if ((p_currentlyDrawnObjects->at(i).P1P4 > 0.0f) && (p_currentlyDrawnObjects->at(i).P1P4 > 1.5f * p_currentlyDrawnObjects->at(i).arrowLength))
                    {
                        ImVec2 pointC(  p_currentlyDrawnObjects->at(i).objectPoints[3].x - (p_currentlyDrawnObjects->at(i).arrowLength * (p_currentlyDrawnObjects->at(i).objectPoints[3].x - p_currentlyDrawnObjects->at(i).objectPoints[2].x))/p_currentlyDrawnObjects->at(i).P1P4,
                                        p_currentlyDrawnObjects->at(i).objectPoints[3].y - (p_currentlyDrawnObjects->at(i).arrowLength * (p_currentlyDrawnObjects->at(i).objectPoints[3].y - p_currentlyDrawnObjects->at(i).objectPoints[2].y))/p_currentlyDrawnObjects->at(i).P1P4);
                        ImVec2 pointD(  pointC.x + (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[3].y - p_currentlyDrawnObjects->at(i).objectPoints[2].y))/p_currentlyDrawnObjects->at(i).P1P4,
                                        pointC.y - (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[3].x - p_currentlyDrawnObjects->at(i).objectPoints[2].x))/p_currentlyDrawnObjects->at(i).P1P4);
                        ImVec2 pointE(  pointC.x - (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[3].y - p_currentlyDrawnObjects->at(i).objectPoints[2].y))/p_currentlyDrawnObjects->at(i).P1P4,
                                        pointC.y + (p_currentlyDrawnObjects->at(i).arrowWidth*(p_currentlyDrawnObjects->at(i).objectPoints[3].x - p_currentlyDrawnObjects->at(i).objectPoints[2].x))/p_currentlyDrawnObjects->at(i).P1P4);

                        p_drawList->PathClear();
                        p_drawList->PathLineTo(ImVec2(pointD.x + mp_TextCanvas->image_pos.x, pointD.y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(p_currentlyDrawnObjects->at(i).objectPoints[3].x + mp_TextCanvas->image_pos.x, p_currentlyDrawnObjects->at(i).objectPoints[3].y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathLineTo(ImVec2(pointE.x + mp_TextCanvas->image_pos.x, pointE.y + mp_TextCanvas->image_pos.y));
                        p_drawList->PathStroke(getBackgroundColor(i), false, p_currentlyDrawnObjects->at(i).thickness);

                    }
                break;

                case RANDOM_LINE :
                    for (unsigned int i = 0 ; i < p_currentlyDrawnObjects->size(); i++)
                    {
                        for (int j = 0 ; j < p_currentlyDrawnObjects->at(i).objectPoints.size() ; j++)
                        {
                            if (p_currentlyDrawnObjects->at(i).anObjectType == RANDOM_LINE)
                                p_drawList->AddCircleFilled(ImVec2(mp_TextCanvas->image_pos.x + p_currentlyDrawnObjects->at(i).objectPoints[j].x,
                                                                   mp_TextCanvas->image_pos.y + p_currentlyDrawnObjects->at(i).objectPoints[j].y),
                                                            p_currentlyDrawnObjects->at(i).thickness,
                                                            getBackgroundColor(i),//p_currentlyDrawnObjects->at(i).objBackgroundColor,
                                                            8);
                        }

                    }

                break;

                default:
                break;
            }
        }
    }
    p_drawList->PopClipRect();

    return 0;
}

// Other possibility coul have been zero-rule algorithm : https://en.wikipedia.org/wiki/Nonzero-rule)
// Desserves a read : https://erich.realtimerendering.com/ptinpoly/

// Use even-odd rule, as a compromise inspired from
// https://stackoverflow.com/questions/11716268/point-in-polygon-algorithm/20868925#20868925
// More information :
// https://en.wikipedia.org/wiki/Even%E2%80%93odd_rule

bool md::Canvas::remove(unsigned int position)
{
    p_currentlyDrawnObjects->erase(p_currentlyDrawnObjects->begin() + position);
    return true;
}


bool md::Canvas::moveObjectTo(unsigned int positionInStack, int choice)
{
    switch(choice)
    {
        case PUT_OBJECT_ON_TOP:
        if (positionInStack < (p_currentlyDrawnObjects->size() - 1))
        {
            for (unsigned int position = positionInStack ; position < (p_currentlyDrawnObjects->size() - 1) ; position++)
            {
                std::swap(currentlyDrawnObjects[position], currentlyDrawnObjects[position+1]);
            }
        }
        break;

        case ONE_LEVEL_DOWN:

        if (positionInStack > 0)
            std::swap(currentlyDrawnObjects[positionInStack-1], currentlyDrawnObjects[positionInStack]);

        break;

        case ONE_LEVEL_UP:

        if (positionInStack < (p_currentlyDrawnObjects->size()-1))
            std::swap(currentlyDrawnObjects[positionInStack], currentlyDrawnObjects[positionInStack+1]);

        break;

        case PUT_OBJECT_AT_THE_BOTTOM :
        if (positionInStack > 0)
        {
            for (unsigned int position = positionInStack ; position > 0 ; position--)
            {
                std::swap(currentlyDrawnObjects[position-1], currentlyDrawnObjects[position]);
            }
        }
        break;

        case DELETE_OBJECT:
            remove(positionInStack);
        break;

        default:
        break;
    }

    return true;
}

bool md::Canvas::insideCircle(ImVec2 mousePos, ImVec2 center, float R2)
{
   if ( ((mousePos.x - center.x)*(mousePos.x - center.x) + (mousePos.y - center.y)*(mousePos.y - center.y)) <= R2 )
       return true;
   else
       return false;
}

bool md::Canvas::intersectEmptyCircle(ImVec2 mousePos, ImVec2 center, float R2_in, float R2_out)
{
    if (insideCircle(mousePos, center, R2_out) && (!insideCircle(mousePos, center, R2_in)))
        return true;
    else
        return false;
}

bool md::Canvas::intersectSegment(ImVec2 mousePos, ImVec2 p_A, ImVec2 p_B)
{
    // "far, close, inside" algorithm  Author : ericb  Sept 2019
    bool toReturn = false;

    float minX = ImMin(p_A.x, p_B.x);
    float maxX = ImMax(p_A.x, p_B.x);
    float minY = ImMin(p_A.y, p_B.y);
    float maxY = ImMax(p_A.y, p_B.y);

    if ((minX > mousePos.x) || ( maxX < mousePos.x))
        return false;

    if ((minY > mousePos.y) || ( maxY < mousePos.y))
        return false;

    if ( (mousePosIsPoint(mousePos, p_A) || (mousePosIsPoint(mousePos,p_B))) )
        return true;

    if ( (((p_A.x - mousePos.x)*( p_B.y - p_A.y) - (p_A.y - mousePos.y)*(p_B.x - p_A.x)) <= EPSILON)
         && ((mousePos.x - p_A.x)*(mousePos.x - p_B.x) + (mousePos .y - p_A.y)*(mousePos.y - p_B.y)  < 0) )
        toReturn = true;
    else
        toReturn = false;

    return toReturn;
}

bool md::Canvas::mousePosIsPoint (ImVec2 mousePos, ImVec2 point)
{
    bool toReturn = false;

    if ((mousePos.x == point.x) && (mousePos.y == point.y))
        toReturn = true;
    else
        toReturn = false;

    return toReturn;
}


bool md::Canvas::insideSimpleArrow(ImVec2 mousePos, ImVector<ImVec2> polygon, ImVector<ImVec2> arrowPolygon)
{
    bool toReturn = false;

    if (   intersectSegment(mousePos, polygon[0], polygon[1])
        || intersectSegment(mousePos, polygon[1], arrowPolygon[1])
        || intersectSegment(mousePos, polygon[1], arrowPolygon[2]))
    {
        toReturn = true;
    }
    else
        toReturn = false;

    return toReturn;
}

bool md::Canvas::insidePolygon(ImVec2 mousePos, ImVector<ImVec2> polygon)
{
    float minX = polygon[ 0 ].x;
    float maxX = polygon[ 0 ].x;
    float minY = polygon[ 0 ].y;
    float maxY = polygon[ 0 ].y;

    // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
    // Working link : https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
    bool inside = false;

    for ( int i = 1 ; i < polygon.size() ; i++ )
    {
        ImVec2 q = polygon[ i ];

        minX = ImMin( q.x, minX );
        maxX = ImMax( q.x, maxX );
        minY = ImMin( q.y, minY );
        maxY = ImMax( q.y, maxY );
    }

    // outside of the whole bound
    if ( mousePos.x < minX || mousePos.x > maxX || mousePos.y < minY || mousePos.y > maxY )
        return false;

    for ( int i = 0, j = polygon.size() - 1 ; i < polygon.size() ; j = i++ )
    {
        //  s/>/>=/  + s/</<=/ to add the cases point is exactly a vertex of the polygon
        if ( ( polygon[ i ].y >= mousePos.y ) != ( polygon[ j ].y >= mousePos.y ) &&
             mousePos.x <= ( polygon[ j ].x - polygon[ i ].x ) * ( mousePos.y - polygon[ i ].y ) / ( polygon[ j ].y - polygon[ i ].y ) + polygon[ i ].x )
        {
            inside = !inside;
        }
    }
    return inside;
}

bool md::Canvas::insideFilledRectangle(ImVec2 mousePos, ImVector <ImVec2> Points)
{
    bool toReturn = false;

    float Xmax = ImMax(Points[0].x, Points[1].x);
    float Xmin = ImMin(Points[0].x, Points[1].x);
    float Ymax = ImMax(Points[0].y, Points[1].y);
    float Ymin = ImMin(Points[0].y, Points[1].y);

    if (fabs((Points[0].x - Points[1].x)*(Points[0].y - Points[1].y)) < EPSILON )
        return intersectSegment(mousePos, Points[0], Points[1]);

    if ((mousePos.x < Xmin) || (mousePos.x > Xmax) || (mousePos.y < Ymin)|| (mousePos.y > Ymax))
        toReturn = false;
    else
        toReturn = true;

    return toReturn;
}


bool md::Canvas::intersectEmptyRectangle(ImVec2 mousePos, ImVector <ImVec2> Rect_ext, ImVector <ImVec2> Rect_int)
{
    bool toReturn = false;

    if (insidePolygon(mousePos, Rect_ext) && !(insidePolygon(mousePos, Rect_int)))

        toReturn = true;
    else
        toReturn = false;

    return toReturn;
}


bool md::Canvas::insideEllipse(ImVec2 mousePos, float long_axis, ImVec2 F1, ImVec2 F2)
{
    bool toReturn = false;

    float F1P = sqrtf( ((mousePos.x - F1.x)*(mousePos.x - F1.x)) + ((mousePos.y - F1.y)*(mousePos.y - F1.y)) );
    float PF2 = sqrtf( ((mousePos.x - F2.x)*(mousePos.x - F2.x)) + ((mousePos.y - F2.y)*(mousePos.y - F2.y)) );

    if ((F1P + PF2) <= (2*long_axis))
        toReturn = true;
    else
        toReturn = false;

    return toReturn;
}


bool md::Canvas::intersectEmptyEllipse(ImVec2 mousePos, float long_axis, ImVec2 F1, ImVec2 F2, float thickness)
{
    bool toReturn = false;

    if (insideEllipse( mousePos, long_axis + thickness, F1, F2) && !(insideEllipse(mousePos, long_axis - thickness, F1, F2)))
        toReturn = true;
    else
        toReturn = false;

    return toReturn;
}


// END CANVAS
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
