/* fonts.cpp
 * Copyright Eric Bachard  2022 April 24th / Licence GPL v2
 * This file is under GPL v2 license
 * contains arrays of fontnames the user can select
 * between them another Microsoft Windows Font used in his application
 * (just in case the font is broken, what occurs regularly)
 */

#include "fonts.hpp"

#ifdef NATIVE_BUILD

/* Unix fonts */

const char * SystemFontFile[SYSTEM_FONT_MAX] =
{
    UNIX_TTF_FONT_DROID_SANS            ,
    UNIX_TTF_FONT_UBUNTU_REGULAR        ,
    UNIX_TTF_FONT_UBUNTU_BOLD           ,
    UNIX_TTF_FONT_COMIC_SANS_MS         ,
    UNIX_TTF_FONT_COMIC_SANS_MS_BOLD    ,
    UNIX_TTF_FONT_ARIAL                 ,
    UNIX_TTF_FONT_ARIAL_BLACK           ,
    UNIX_TTF_FONT_VERDANA               ,
    UNIX_TTF_FONT_TIMES_NEW_ROMAN       ,
    UNIX_TTF_FONT_IMPACT
};

const char* font_items[] = { "DroidSans"      , "Ubuntu-Regular", "Ubuntu-Bold", "Comic Sans",
                             "Comic Sans bold", "Arial"         , "Arial black", "Verdana"   ,
                             "Times New Roman", "Impact"
                           };

#else

// SOURCE : https://docs.microsoft.com/en-us/typography/fonts/windows_7_font_list

/* Windows system fonts */

const char * SystemFontFile[SYSTEM_FONT_MAX] =
{
    WINDOWS_TTF_FONT_ARIAL              ,
    WINDOWS_TTF_FONT_ARIAL_ITALIC       ,
    WINDOWS_TTF_FONT_ARIAL_BOLD         ,
    WINDOWS_TTF_FONT_ARIAL_BLACK        ,
    WINDOWS_TTF_FONT_CALIBRI            ,
    WINDOWS_TTF_FONT_COMIC_SANS_MS      ,
    WINDOWS_TTF_FONT_COMIC_SANS_MS_BOLD ,
    WINDOWS_TTF_FONT_TAHOMA             ,
    WINDOWS_TTF_FONT_TAHOMA_BOLD        ,
    WINDOWS_TTF_FONT_TIMES_NEW_ROMAN    ,
    WINDOWS_TTF_FONT_VERDANA            ,
    WINDOWS_TTF_FONT_VERDANA_BOLD       ,
    WINDOWS_TTF_FONT_VERDANA_ITALIC     ,
    WINDOWS_TTF_FONT_WEBDINGS           ,
    WINDOWS_TTF_FONT_WINGDING
};

const char* font_items[] = { "Arial"           , "Arial italique" , "Arial bold"        , "Arial black",
                             "Calibri"         , "Comic Sans MS"  , "Comic Sans MS bold", "Tahoma",
                             "Tahoma_Bold"     , "Times New Roman", "Verdana"           , "Verdana bold",
                             "Verdana italique", "Webdings"       , "Wingding" };

#endif  /* NATIVE_BUILD */

