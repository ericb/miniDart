/*
 * engine.cpp file, from miniDart project
 * Author : Eric Bachard  / lundi 3 octobre 2016, 14:35:03 (UTC+0200)
 * This file is under GPL v2 License
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include "engine.hpp"                  // class Engine
#include "application.hpp"             // WINDOW_WIDTH, WINDOW_HEIGHT,
#include "sdl_utils.hpp"               // getInfo()

#include "SDL2/SDL.h"                  // We use SDL2

#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"

#include <GL/gl.h>                     // OpenGL

#ifdef BUILDING_FRENCH
#include "miniDart_fr.hpp"
#else
#include "miniDart_en-US.hpp"
#endif

using std::cout;
using std::endl;

#ifdef NATIVE_BUILD
// Linux working value

// in case you got an High DPI display (4k or +)
//static const float kSysDefaultDpi = 150.0f;

// Normal case. e.g. laptop with current (2k max) display
static const float kSysDefaultDpi = 96.0f;
#else
// Windows value. Needs more love ...
static const float kSysDefaultDpi = 96.0f;

#endif

// taken from https://nlguillemot.wordpress.com/2016/12/11/high-dpi-rendering/
//#if defined(_WIN32)
void Engine::getSystemDisplayDPI(int displayIndex, float * dpi, float * defaultDpi)
{
    if (SDL_GetDisplayDPI(displayIndex, NULL, dpi, NULL) != 0)
    {
        if (dpi)
           *dpi = kSysDefaultDpi; // Failed to get DPI, so just return the default value.
    }
    // FIXME Looks buggy, bizarre :-/
//    if (defaultDpi)
//        *defaultDpi = kSysDefaultDpi;
}
//#endif

// Try setting the value on the fly
bool Engine::setApplicationDisplayDPI(float newDpi)
{
    dpi = newDpi;

    if ((int)getDpi() != (int)newDpi)
    {
        windowDpiScaledWidth = int(windowDpiUnscaledWidth * dpi / defaultDpi);
        windowDpiScaledHeight = int(windowDpiUnscaledHeight * dpi / defaultDpi);

        // TODO : if window is created with SDL_WINDOW_ALLOW_HIGHDPI, we'll have to change
        // the method using SDL_GL_GetDrawableSize() or SDL_GetRendererOutputSize() to get
        // the real client area size in pixels
        SDL_SetWindowSize(getWindow(), windowDpiScaledWidth, windowDpiScaledHeight);

        if ((int)getDpi()==(int)newDpi)
            return true;
    }
    // dpi not modified
    return false;
}


Engine::Engine()
{
    int anErr = init_SDL();

    if (anErr != 0)
    {
        std::cerr << "Cannot initialize SDL or OpenGL. Exiting" << std::endl;
        clean_and_close();
    }
}


Engine::~Engine()
{
    clean_and_close();
}


int Engine::init_SDL()
{
    std::cout << "\n";
    SDL_Log("AVX     %s\n", SDL_HasAVX()? "detected" : "not detected");
    SDL_Log("AVX2    %s\n", SDL_HasAVX2()? "detected" : "not detected");
    SDL_Log("AVX512F %s\n", SDL_HasAVX512F()? "detected" : "not detected");
    std::cout << "\n";

    // ericb 2022 08 19
    // source :
    // https://answers.opencv.org/question/120699/can-opencv-310-be-set-to-capture-an-rtsp-stream-over-udp/
    //   setenv("OPENCV_FFMPEG_CAPTURE_OPTIONS", "rtsp_transport;udp", 1);

#ifdef NATIVE_BUILD
    // https://github.com/libsdl-org/SDL/issues/2917
    putenv((char *)"SDL_PULSEAUDIO_INCLUDE_MONITORS=true");
#endif
    // https://www.gog.com/forum/thimbleweed_park/linux_unable_to_init_sdl_mixer_alsa_couldnt_open_audio_device_no_such_device
    // https://wiki.libsdl.org/FAQUsingSDL
#ifndef NATIVE_BUILD
    SDL_setenv("SDL_AUDIODRIVER", "DirectSound", true);
    putenv((char *)"SDL_AUDIODRIVER=DirectSound");
#else
    // SDL_setenv("SDL_AUDIODRIVER", "alsa", true);
    putenv((char *)"SDL_AUDIODRIVER=alsa");
    /// INSTANT CRASH  ?
    ///putenv((char *)"SDL_AUDIODRIVER=pipewire");
//    putenv((char *)"SDL_AUDIODEV=pulse");
#endif

    // about issues between OpenGL 3.3. and OpenGL 3.0 (only Linux concerned)
    // https://discourse.libsdl.org/t/confused-about-what-opengl-context-is-being-used-with-sdl/22860
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        std::cerr << "Failed to initialize SDL: " << SDL_GetError() << std::endl;
//        return 1;
    }

    // removed for TESTS, and no apparent change at least on Linux ...
    // SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);

    // avoid using compatiblity profile
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    // 3.2 core profile should be sufficient (true on Windows)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

#ifdef NATIVE_BUILD
// TEST. source : https://discourse.libsdl.org/t/stop-sdl2-telling-linux-to-disable-desktop-compositor/23206/5
    SDL_SetHint(SDL_HINT_VIDEO_X11_NET_WM_BYPASS_COMPOSITOR, "0");

    //SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengles");
#endif
    SDL_GetCurrentDisplayMode(0, &current);

    SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );

    // 1 == enable VSync ; 0 == disable VSync
    #define USE_VSYNC
    #ifdef USE_VSYNC
    SDL_GL_SetSwapInterval(1);
    #else
    SDL_GL_SetSwapInterval(0);
    #endif

    window = nullptr;

// HIGH DPI FOR ALL !

    mWidth = WINDOW_WIDTH;
    mHeight = WINDOW_HEIGHT;

    windowDpiScaledWidth    = WINDOW_WIDTH;
    windowDpiScaledHeight   = WINDOW_HEIGHT;
    windowDpiUnscaledWidth  = WINDOW_WIDTH;
    windowDpiUnscaledHeight = WINDOW_HEIGHT;

    dpi          = kSysDefaultDpi;
    defaultDpi   = kSysDefaultDpi;
    displayIndex = 0;

    getSystemDisplayDPI(displayIndex, &dpi, &defaultDpi);

    windowDpiScaledWidth = int(windowDpiUnscaledWidth * dpi / defaultDpi);
    windowDpiScaledHeight = int(windowDpiUnscaledHeight * dpi / defaultDpi);

    std::cerr << "dpi                   : " << dpi << "\n";
    std::cerr << "defaultDpi            : " << defaultDpi << "\n";

    std::cerr << "windowDpiUnscaledWidth  : " << windowDpiUnscaledWidth << "\n";
    std::cerr << "windowDpiUnscaledHeight : " << windowDpiUnscaledHeight << "\n";

    std::cerr << "windowDpiScaledWidth  : " << windowDpiScaledWidth << "\n";
    std::cerr << "windowDpiScaledHeight : " << windowDpiScaledHeight << "\n";

    window = SDL_CreateWindow(MINIDART_VERSION_NUMBER,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              windowDpiScaledWidth,
                              windowDpiScaledHeight,
                              SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE|SDL_WINDOW_ALLOW_HIGHDPI);
    // check whether the SDL2 window exists
    if (getWindow() == nullptr)
        sdl_application_abort("Problem creating the SDL window.\n");
    else
        std::cout << "SDL2 Window created " << "\n";

    SDL_GetWindowSize(getWindow(), &mWidth, &mHeight);

    std::cout << "SDL_HAS_CAPTURE_AND_GLOBAL_MOUSE    SDL_VERSION_ATLEAST(2,0,4) = " << SDL_VERSION_ATLEAST(2,0,4) << "\n";
    std::cout << "SDL_HAS_WINDOW_ALPHA                SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << "\n";
    std::cout << "SDL_HAS_ALWAYS_ON_TOP               SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << "\n";
    std::cout << "SDL_HAS_USABLE_DISPLAY_BOUNDS       SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << "\n";
    std::cout << "SDL_HAS_PER_MONITOR_DPI             SDL_VERSION_ATLEAST(2,0,4) = " << SDL_VERSION_ATLEAST(2,0,4) << "\n";
    std::cout << "SDL_HAS_VULKAN                      SDL_VERSION_ATLEAST(2,0,6) = " << SDL_VERSION_ATLEAST(2,0,6) << "\n";
    std::cout << "SDL_HAS_MOUSE_FOCUS_CLICKTHROUGH    SDL_VERSION_ATLEAST(2,0,5) = " << SDL_VERSION_ATLEAST(2,0,5) << "\n";

    //    seems to work very well. Just an issue : how to handle the window and move it ? (ALT + is suboptimal)
    //    SDL_SetWindowBordered(window, SDL_FALSE);

    const SDL_GLContext & glcontext = SDL_GL_CreateContext(getWindow());

    if (glcontext == nullptr)
        sdl_application_abort("Problem creating GL context.\n");

#ifdef IMGUI_IMPL_OPENGL_LOADER_GLEW
    glewExperimental = GL_TRUE;
    std::cerr << "\nUsing STATIC GLEW loader" << "\n";

    if (glewInit() != GLEW_OK)
#else
    std::cerr << "\nUsing implemented GL3W loader" << "\n";

    if (gl3wInit() != 0)
#endif
    {
        SDL_GL_DeleteContext(glcontext);
        SDL_Quit();
        throw std::runtime_error("Failed to initialize OpenGL\n");
    }

    std::cout << "\nImGui version = " <<  IMGUI_VERSION  << "\n\n";

    getInfo();

#ifdef NATIVE_BUILD
#endif
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_MULTISAMPLE);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    int drawable_width, drawable_height;
    SDL_GL_GetDrawableSize(getWindow(), &drawable_width, &drawable_height);

    // FIXME : implement AudioDevicesList class 
    int count = 8;

    for (int i = 0; i < count; ++i) {
        fprintf(stdout, "Audio device (no recording capability)   %d: %s\n", i, SDL_GetAudioDeviceName(i, 0));
        fprintf(stdout, "Audio device (with recording capability) %d: %s\n", i, SDL_GetAudioDeviceName(i, 1));
    }

    audioMgr.init();

    // FIXME use me
    // const md::AudioManager & p_audioMgr = audioMgr;

    const char* driver_name = SDL_GetCurrentAudioDriver();

    if (driver_name) {
        printf("Audio subsystem initialized; driver = %s.\n", driver_name);
    } else {
        printf("Audio subsystem not initialized.\n");
    }

    return EXIT_SUCCESS;
}

void Engine::clean_and_close()
{
    SDL_DestroyWindow(getWindow());
    window = nullptr;
    SDL_GL_DeleteContext(glcontext);
    SDL_Quit();
}





