/*
 * File videomanager.cpp belongs to miniDart project
 * Copyright : Eric Bachard  / 2019 april 26th, 14:35:03 (UTC+0200)
 * This document is under GPL v2 license
 * See : http://www.gnu.org/licenses/gpl-2.0.html
 */

#include <videomanager.hpp>

// CTor
md::VideoManager::VideoManager()
{
};

md::VideoManager::~VideoManager()
{
}


bool md::VideoManager::init()
{
    b_recording_video     = false;
    b_full_video_selected = false;
    b_cell_phone_selected = false;

    myFourcc = CV_FOURCC('D','I','V','3');
    myContainer = ".avi";
    outWidth = 1280;
    outHeight = 720;
    cv::Size  outFrameSize(static_cast<int>(outWidth), static_cast<int>(outHeight));
    outFPS = 24.0;

#ifdef FPS_FEATURE_ENABLED
    out_image_format = DEFAULT_IMAGE_FORMAT; // IMAGE_FORMAT_720P
    out_old_framesize = outWidth * outHeight;
    out_new_framesize = outWidth * outHeight;
#endif
    // Default usage is record USB_WEBCAM
    current_source = USB_WEBCAM;
    old_current_source = current_source;

    return true;
}

void md::VideoManager::quit(void)
{
}

int md::VideoManager::startRecordingVideo(void)
{

    return EXIT_SUCCESS;
}


int md::VideoManager::stopRecordingVideo(void)
{

    return EXIT_SUCCESS;
}


int md::VideoManager::startPausingVideo()
{

    return EXIT_SUCCESS;
}

int md::VideoManager::stopPausingVideo()
{

    return EXIT_SUCCESS;
}


float md::VideoManager::initialize_position(void)
{
    float aPosition = 0.0;

    return aPosition;
}

int md::VideoManager::LoadFile(const char * aPath, int aVideoType)
{

    return EXIT_SUCCESS;
}


int md::VideoManager::selectFolder(const char * currentPath)
{
    if (currentPath)
    {
        // do nothing
        std::cout << "currentPath contient : " << currentPath << "\n";
        // strcpy(defaultPath, currentPath);
        std::cout << "defaultPath contient : " << defaultPath << "\n";
    }
    else
    {
        std::cout << "No filename, please choose one." << "\n";
        /* load the file from GUI */
        //char new_path[PATH_MAX];
        pick_folder(defaultPath) ? fprintf(stderr, "User aborted the open file dialog.\n") : selectFolder(defaultPath);
    }

    return EXIT_SUCCESS;
}

