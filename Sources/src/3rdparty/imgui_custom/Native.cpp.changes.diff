diff --git a/src/Mahi/Gui/Native.cpp b/src/Mahi/Gui/Native.cpp
index efea5e8..987784b 100644
--- a/src/Mahi/Gui/Native.cpp
+++ b/src/Mahi/Gui/Native.cpp
@@ -1,9 +1,11 @@
+#ifdef BUILD_NFD_EXT
+#include <Native.hpp>
+#else
 #include <Mahi/Gui/Native.hpp>
-#include <nfd.h>
+#endif
 #include <cstring>
 #include <ctime>
 #include <iomanip>
-#include <filesystem>
 #include <cassert>
 #include <sstream>
 #include <iostream>
@@ -26,20 +28,30 @@
 #include <sys/sysctl.h>
 #endif
 
-namespace fs = std::filesystem;
+// TODO: We need a more robust way to detect where fs may be...
+#ifdef BUILD_NFD_EXT
+    #include <experimental/filesystem>
+    namespace fs = std::experimental::filesystem;
+#else
+    #include <filesystem>
+    namespace fs = std::filesystem;
+#endif
 
-namespace mahi {
-namespace gui {
+namespace native {
 
-DialogResult save_dialog(const std::string &filterList, const std::string &defaultPath, std::string &outPath)
-{
-    nfdchar_t *savePath = NULL;
-    nfdresult_t result = NFD_SaveDialog(filterList.c_str(), defaultPath.length() > 0 ? defaultPath.c_str() : NULL, &savePath);
-    if (result == NFD_OKAY)
-    {
-        outPath = savePath;
-        free(savePath);
-        return DialogResult::DialogOkay;
+DialogResult save_dialog(std::string& out_path, const std::vector<DialogFilter>& filters, const std::string& default_path, const std::string& default_name) {
+    fs::path defpath(default_path);
+    defpath.make_preferred();
+    NFD::Guard nfdGuard;
+    NFD::UniquePathU8 savePath;
+    nfdresult_t result = NFD::SaveDialog(savePath, 
+                                         filters.empty() ? nullptr : &filters[0], 
+                                         (nfdfiltersize_t)filters.size(),
+                                         default_path.empty() ? nullptr : defpath.string().c_str(),
+                                         default_name.c_str());
+    if (result == NFD_OKAY) {
+        out_path = savePath.get();
+        return DialogOkay;
     }
     else if (result == NFD_CANCEL)
         return DialogResult::DialogCancel;
@@ -47,15 +59,18 @@ DialogResult save_dialog(const std::string &filterList, const std::string &defau
         return DialogResult::DialogError;
 }
 
-DialogResult open_dialog(const std::string &filterList, const std::string &defaultPath, std::string &outPath)
-{
-    nfdchar_t *openPath = NULL;
-    nfdresult_t result = NFD_OpenDialog(filterList.c_str(), defaultPath.length() > 0 ? defaultPath.c_str() : NULL, &openPath);
-    if (result == NFD_OKAY)
-    {
-        outPath = openPath;
-        free(openPath);
-        return DialogResult::DialogOkay;
+DialogResult open_dialog(std::string& out_path, const std::vector<DialogFilter>& filters, const std::string& default_path) {
+    fs::path defpath(default_path);
+    defpath.make_preferred();
+    NFD::Guard nfdGuard;
+    NFD::UniquePathU8 openPath;
+    nfdresult_t result = NFD::OpenDialog(openPath, 
+                                         filters.empty() ? nullptr : &filters[0], 
+                                         (nfdfiltersize_t)filters.size(), 
+                                         default_path.empty() ? nullptr : defpath.string().c_str());
+    if (result == NFD_OKAY) {
+        out_path = openPath.get();
+        return DialogOkay;
     }
     else if (result == NFD_CANCEL)
         return DialogResult::DialogCancel;
@@ -63,17 +78,24 @@ DialogResult open_dialog(const std::string &filterList, const std::string &defau
         return DialogResult::DialogError;
 }
 
-DialogResult open_dialog(const std::string &filterList, const std::string &defaultPath, std::vector<std::string> &outPaths)
-{
-    nfdpathset_t pathSet;
-    nfdresult_t result = NFD_OpenDialogMultiple(filterList.c_str(), defaultPath.length() > 0 ? defaultPath.c_str() : NULL, &pathSet);
-    if (result == NFD_OKAY)
-    {
-        std::size_t n = NFD_PathSet_GetCount(&pathSet);
-        outPaths.resize(n);
-        for (std::size_t i = 0; i < n; ++i)
-            outPaths[i] = NFD_PathSet_GetPath(&pathSet, i);
-        NFD_PathSet_Free(&pathSet);
+DialogResult open_dialog(std::vector<std::string>& out_paths, const std::vector<DialogFilter>& filters, const std::string& default_path) {
+    fs::path defpath(default_path);
+    defpath.make_preferred();
+    NFD::Guard nfdGuard;
+    NFD::UniquePathSet openPaths;   
+    nfdresult_t result = NFD::OpenDialogMultiple(openPaths, 
+                                                 filters.empty() ? nullptr : &filters[0], 
+                                                 (nfdfiltersize_t)filters.size(),
+                                                 default_path.empty() ? nullptr : defpath.string().c_str());
+    if (result == NFD_OKAY) {
+        nfdpathsetsize_t numPaths;
+        NFD::PathSet::Count(openPaths, numPaths);
+        out_paths.resize(numPaths);
+        for (nfdpathsetsize_t i = 0; i < numPaths; ++i) {
+            NFD::UniquePathSetPath path;
+            NFD::PathSet::GetPath(openPaths, i, path);
+            out_paths[i] = path.get();
+        }
         return DialogResult::DialogOkay;
     }
     else if (result == NFD_CANCEL)
@@ -82,15 +104,16 @@ DialogResult open_dialog(const std::string &filterList, const std::string &defau
         return DialogResult::DialogError;
 }
 
-DialogResult pick_folder(const std::string &defaultPath, std::string &outPath)
+DialogResult pick_dialog(std::string &out_path, const std::string& default_path)
 {
-    nfdchar_t *pickPath = NULL;
-    nfdresult_t result = NFD_PickFolder(defaultPath.length() > 0 ? defaultPath.c_str() : NULL, &pickPath);
-    if (result == NFD_OKAY)
-    {
-        outPath = pickPath;
-        free(pickPath);
-        return DialogResult::DialogOkay;
+    fs::path defpath(default_path);
+    defpath.make_preferred();
+    NFD::Guard nfdGuard;
+    NFD::UniquePathU8 openPath;
+    nfdresult_t result = NFD::PickFolder(openPath, default_path.empty() ? nullptr : defpath.string().c_str());
+    if (result == NFD_OKAY) {
+        out_path = openPath.get();
+        return DialogOkay;
     }
     else if (result == NFD_CANCEL)
         return DialogResult::DialogCancel;
@@ -98,11 +121,40 @@ DialogResult pick_folder(const std::string &defaultPath, std::string &outPath)
         return DialogResult::DialogError;
 }
 
+namespace {
+struct SysDirStr {
+    SysDirStr(const char* name) {
+        fs::path p(std::string(std::getenv(name)));
+        str = p.generic_string();
+    }
+    std::string str;
+};
+}
+
 ///////////////////////////////////////////////////////////////////////////////
 // WINDOWS
 ///////////////////////////////////////////////////////////////////////////////
 #ifdef _WIN32
 
+const std::string& sys_dir(SysDir dir) {
+    static SysDirStr user("USERPROFILE");
+    static SysDirStr roam("APPDATA");
+    static SysDirStr local("LOCALAPPDATA");
+    static SysDirStr temp("TEMP");
+    static SysDirStr data("PROGRAMDATA");
+    static SysDirStr files("PROGRAMFILES");
+    static SysDirStr x86("PROGRAMFILES(X86)");
+    switch (dir) {
+        case SysDir::UserProfile: return user.str; break;
+        case SysDir::AppDataRoaming: return roam.str; break;
+        case SysDir::AppDataLocal: return local.str; break;
+        case SysDir::AppDataTemp: return temp.str; break;
+        case SysDir::ProgramData: return data.str; break;
+        case SysDir::ProgramFiles: return files.str; break;
+        case SysDir::ProgramFilesX86: return x86.str; break;
+    }
+}
+
 bool open_folder(const std::string &path)
 {
     fs::path p(path);
@@ -138,14 +190,20 @@ void open_email(const std::string &address, const std::string &subject)
     ShellExecuteA(0, 0, str.c_str(), 0, 0, 5);
 }
 
-#elif (__APPLE__)
+#elif defined (__APPLE__)
 
 ///////////////////////////////////////////////////////////////////////////////
 // macOS
 ///////////////////////////////////////////////////////////////////////////////
 
+const std::string& sys_dir(SysDir dir) {
+    static std::string todo = "TODO,SORRY";
+    return todo;
+}
+
 bool open_folder(const std::string &path)
 {
+    int anErr = 0;
     fs::path p(path);
     if (fs::exists(p) && fs::is_regular_file(p))
     {
@@ -158,6 +216,7 @@ bool open_folder(const std::string &path)
 
 bool open_file(const std::string &path)
 {
+    int anErr = 0;
     fs::path p(path);
     if (fs::exists(p) && fs::is_directory(p))
     {
@@ -170,6 +229,7 @@ bool open_file(const std::string &path)
 
 void open_url(const std::string &url)
 {
+    int anErr = 0;
     std::string command = "open " + url;
     system(command.c_str());
 }
@@ -181,10 +241,73 @@ void open_email(const std::string &address, const std::string &subject)
     system(command.c_str());
 }
 
+#elif defined(__linux__)
+
+static int anErr = 0;
+
+///////////////////////////////////////////////////////////////////////////////
+// Linux
+///////////////////////////////////////////////////////////////////////////////
+
+const std::string& sys_dir(SysDir dir) {
+    static std::string todo = "TODO,SORRY";
+    return todo;
+}
+
+bool open_folder(const std::string &path)
+{
+    fs::path p(path);
+    if (fs::exists(p) && fs::is_regular_file(p))
+    {
+        std::string command = "open " + p.generic_string();
+        anErr = system(command.c_str());
+        if (anErr != 0)
+            std::cout << "Pb with open_folder()" << "\n";
+        return true;
+    }
+    return false;
+}
+
+bool open_file(const std::string &path)
+{
+    fs::path p(path);
+    if (fs::exists(p) && fs::is_directory(p))
+    {
+        std::string command = "open " + p.generic_string();
+        anErr = system(command.c_str());
+
+        if (anErr != 0)
+            std::cout << "Pb with open_file()" << "\n";
+
+        return true;
+    }
+    return false;
+}
+
+void open_url(const std::string &url)
+{
+    std::string command = "open " + url;
+
+    anErr = system(command.c_str());
+
+    if (anErr != 0)
+        std::cout << "Pb with open_url()" << "\n";
+}
+
+void open_email(const std::string &address, const std::string &subject)
+{
+    std::string mailTo = "mailto:" + address + "?subject=" + subject; // + "\\&body=" + bodyMessage;
+    std::string command = "open " + mailTo;
+    anErr = system(command.c_str());
+
+    if (anErr != 0)
+        std::cout << "Pb with open_url()" << "\n";
+}
+
 #endif
 
-} // namespace gui
-} // namesapce mahi
+
+} // namespace native
 
 // Links/Resources
 // https://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
