// dear imgui: standalone example application for SDL2 + OpenGL
// If you are new to dear imgui, see examples/README.txt and documentation at the top of imgui.cpp.
// (SDL is a cross-platform general purpose library for handling windows, inputs, OpenGL/Vulkan/Metal graphics context creation, etc.)
// (GL3W is a helper library to access OpenGL functions since there is no standard header to access modern OpenGL functions easily. Alternatives are GLEW, Glad, etc.)

#include "imgui.h"
#include "imgui_impl_sdl.h"
#include "imgui_impl_opengl3.h"
#include <stdio.h>
#include <SDL.h>
#include "imgui_fade_in_out.hpp"

// About Desktop OpenGL function loaders:
//  Modern desktop OpenGL doesn't have a standard portable header file to load OpenGL function pointers.
//  Helper libraries are often used for this purpose! Here we are supporting a few common ones (gl3w, glew, glad).
//  You may use another loader/header of your choice (glext, glLoadGen, etc.), or chose to manually implement your own.
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
#include <GL/gl3w.h>            // Initialize with gl3wInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
#include <GL/glew.h>            // Initialize with glewInit()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
#include <glad/glad.h>          // Initialize with gladLoadGL()
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/Binding.h>  // Initialize with glbinding::Binding::initialize()
#include <glbinding/gl/gl.h>
//using namespace gl;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
#define GLFW_INCLUDE_NONE       // GLFW including OpenGL headers causes ambiguity or multiple definition errors.
#include <glbinding/glbinding.h>// Initialize with glbinding::initialize()
#include <glbinding/gl/gl.h>
//using namespace gl;
#endif
//#else
//#include IMGUI_IMPL_OPENGL_LOADER_CUSTOM
//#endif

int main(int, char**)
{

    // Setup SDL
    // (Some versions of SDL before <2.0.10 appears to have performance/stalling issues on a minority of Windows systems,
    // depending on whether SDL_INIT_GAMECONTROLLER is enabled or disabled.. updating to latest version of SDL is recommended!)
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_GAMECONTROLLER) != 0)
    {
        printf("Error: %s\n", SDL_GetError());
        return -1;
    }

    // Decide GL+GLSL versions
#if __APPLE__
    // GL 3.2 Core + GLSL 150
    const char* glsl_version = "#version 150";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

    // Create window with graphics context
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_WindowFlags window_flags = (SDL_WindowFlags)(SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    SDL_Window* window = SDL_CreateWindow("Dear ImGui SDL2+OpenGL3 example", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, window_flags);
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync

    // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
    bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
    bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
    bool err = gladLoadGL() == 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING2)
    bool err = false;
    glbinding::Binding::initialize();
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLBINDING3)
    bool err = false;
    glbinding::initialize([](const char* name) { return (glbinding::ProcAddress)SDL_GL_GetProcAddress(name); });
#else
    bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
    if (err)
    {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Load Fonts
    // - If no fonts are loaded, dear imgui will use the default font. You can also load multiple fonts and use ImGui::PushFont()/PopFont() to select them.
    // - AddFontFromFileTTF() will return the ImFont* so you can store it if you need to select the font among multiple.
    // - If the file cannot be loaded, the function will return NULL. Please handle those errors in your application (e.g. use an assertion, or display an error and quit).
    // - The fonts will be rasterized at a given size (w/ oversampling) and stored into a texture when calling ImFontAtlas::Build()/GetTexDataAsXXXX(), which ImGui_ImplXXXX_NewFrame below will call.
    // - Read 'docs/FONTS.txt' for more instructions and details.
    // - Remember that in C/C++ if you want to include a backslash \ in a string literal you need to write a double backslash \\ !
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Roboto-Medium.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/Cousine-Regular.ttf", 15.0f);
    io.Fonts->AddFontFromFileTTF("../../misc/fonts/DroidSans.ttf", 19.0f);
    //io.Fonts->AddFontFromFileTTF("../../misc/fonts/ProggyTiny.ttf", 10.0f);
    //ImFont* font = io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
    //IM_ASSERT(font != NULL);

    // Our state
    bool show_demo_window = true;
    bool show_another_window = false;
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    // fade in-out effect with windows transparency
    md::FadeInOut fade_in_out;
    fade_in_out.init();

    md::FadeInOut heartbeat;
    heartbeat.init();


    // Main loop
    bool done = false;
    while (!done)
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                done = true;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window))
                done = true;
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();

        static float min = 0.0f;
        static float max = 1.0f;
        static float up_duration = 0.4f;
        static float down_duration = 1.0f;

        static float min_hb = 0.0f;
        static float max_hb = 1.0f;
        static float up_duration_hb = 0.5f;
        static float down_duration_hb = 1.0f;

        static bool b_child_window_visible = false;

        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);

        // 2. Show a simple window that we create ourselves. We use a Begin/End pair to created a named window.
        {
            static float f = 0.0f;
            static int counter = 0;
            static bool b_inside_window = false;

            static float opacity = 1.0f;
            bool open = true;
            style.WindowBorderSize = 0.0f;

            ImGuiWindowFlags flags = ImGuiWindowFlags_NoDecoration;
            ImGui::SetNextWindowBgAlpha(opacity);

            ImGui::Begin("Hello, world!", &open, flags);                          // Create a window called "Hello, world!" and append into it.

            ImDrawList * draw = ImGui::GetWindowDrawList();
            ImVec2 wpos = ImGui::GetCursorScreenPos();
/*
            draw->AddRect( ImVec2( wpos.x + 0, wpos.y + 0), ImVec2( wpos.x + 10, wpos.y + 10 ), 0xFFFFFFFF );
            draw->AddLine( ImVec2( wpos.x + 2, wpos.y + 2), ImVec2( wpos.x + 3 ,  wpos.y + 2  ), 0xFF0000FF );
            draw->AddLine( ImVec2( wpos.x + 4, wpos.y + 2), ImVec2( wpos.x + 8 ,  wpos.y + 2  ), 0xFF00FF00 );
            draw->AddLine( ImVec2( wpos.x + 2, wpos.y + 3), ImVec2( wpos.x + 2 ,  wpos.y + 7  ), 0xFF00FFFF );
*/

            float width = 100.0f;
            float height = 100.0f;
            ImVec2 orig = ImVec2( 100.0f, 40.0f);
            //ImVec2 size = ImVec2( width, height);

            // Colors : 0 x A B G R
            draw->AddRect( ImVec2( wpos.x + orig.x, wpos.y + orig.y), ImVec2( wpos.x + orig.x + width, wpos.y + orig.y + height), 0xFFFFFFFF );
            draw->AddRectFilled( ImVec2( wpos.x + orig.x, wpos.y + orig.y), ImVec2( wpos.x + orig.x + width, wpos.y + orig.y + height), 0xFFFF0000 );
            draw->AddLine( ImVec2( wpos.x + orig.x+ 2, wpos.y + orig.y + 2), ImVec2( wpos.x + orig.x + 3 ,  wpos.y + orig.y + 2  ), 0xFF0000FF );
//            draw->AddLine( ImVec2( wpos.x + 4, wpos.y + 2), ImVec2( wpos.x + 8 ,  wpos.y + 2  ), 0xFF00FF00 );
//            draw->AddLine( ImVec2( wpos.x + 2, wpos.y + 3), ImVec2( wpos.x + 2 ,  wpos.y + 7  ), 0xFF00FFFF );


//  FADE IN / OUT
            // Later use ? (e.g. multi screen ?)
            //ImGuiViewport* viewport = ImGui::GetMainViewport();
            //ImVec2 work_area_pos = viewport->Pos;//GetWorkPos();   // Instead of using viewport->Pos we use GetWorkPos() to avoid menu bars, if any!
            //ImVec2 work_area_size = viewport->GetWorkSize();
            ImVec2 window_pos = ImGui::GetWindowPos();
            ImVec2 window_size = ImGui::GetContentRegionMax();  // Other possible use : ImGui::GetContentRegionAvail();
            ImVec2 mouse_pos = ImVec2(ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);


            if (((mouse_pos.x < window_pos.x)||(mouse_pos.x > (window_pos.x + window_size.x))||
            (mouse_pos.y < window_pos.y)||(mouse_pos.y > (window_pos.y + window_size.y))) && (b_child_window_visible == false))
            {
                b_inside_window = false;
            }
            else
                b_inside_window = true;

            opacity = fade_in_out.fadeInOut(up_duration, down_duration, min, max, b_inside_window);
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, opacity);

// END FADE IN / OUT

            ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
            ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
            ImGui::Checkbox("Another Window", &show_another_window);

            ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f

            ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color

            if (ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows|ImGuiHoveredFlags_AllowWhenBlockedByPopup|ImGuiHoveredFlags_AllowWhenBlockedByActiveItem))
                b_child_window_visible = true;
            else
                b_child_window_visible = false;


            if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
                counter++;
            ImGui::SameLine();
            ImGui::Text("counter = %d", counter);

            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

            ImGui::Text("heartbeat effect parameters");
            ImGui::SliderFloat("Mini hb opacity ", &min_hb, 0.0f , 1.0f, "%.1f"  );
            ImGui::SliderFloat("Maxi hb opacity", &max_hb, 0.0f , 1.0f, "%.1f"  );
            ImGui::SliderFloat("Fade in heartbeat time", &up_duration_hb, 0.1f , 2.0f, "%.1f"  );
            ImGui::SliderFloat("Fade out heartbeat time", &down_duration_hb, 0.1f , 2.5f, "%.1f" );

            ImGui::Text("Fade in/out effect parameters");
            ImGui::SliderFloat("Min fade out", &min, 0.0f , 1.0f, "%.1f"  );
            ImGui::SliderFloat("Maxi fade in", &max, 0.0f , 1.0f, "%.1f"  );
            ImGui::SliderFloat("Fade in time", &up_duration, 0.1f , 2.0f, "%.1f"  );
            ImGui::SliderFloat("Fade out time", &down_duration, 0.1f , 2.5f, "%.1f" );

            //ImGui::Checkbox("");

            //ImGui::PopStyleVar(2);
            ImGui::PopStyleVar();

            ImGui::End();
        }
        style.WindowBorderSize = 1.0f;

        // 3. Show another simple window.


        if (show_another_window)
        {

            // Experimental : without menubar. Drawback : not movable ...
            // FIXME-VIEWPORT: Select a default viewport
            //const float DISTANCE = 10.0f;
            //static int corner = 0;
/*
            if (corner != -1)
            {
                ImGuiViewport* viewport = ImGui::GetMainViewport();
                ImVec2 work_area_pos = viewport->GetWorkPos();   // Instead of using viewport->Pos we use GetWorkPos() to avoid menu bars, if any!
                ImVec2 work_area_size = viewport->GetWorkSize();
                ImVec2 window_pos = ImVec2((corner & 1) ? (work_area_pos.x + work_area_size.x - DISTANCE) : (work_area_pos.x + DISTANCE), (corner & 2) ? (work_area_pos.y + work_area_size.y - DISTANCE) : (work_area_pos.y + DISTANCE));
                ImVec2 window_pos_pivot = ImVec2((corner & 1) ? 1.0f : 0.0f, (corner & 2) ? 1.0f : 0.0f);
                ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
                ImGui::SetNextWindowViewport(viewport->ID);
            }
*/
            ImGui::Begin("Another Window", &show_another_window);

            bool b_inside_window_hb = false;

            ImVec2 window_pos_hb = ImGui::GetWindowPos();
            ImVec2 window_size_hb = ImGui::GetWindowSize();
            ImVec2 mouse_pos_hb = ImVec2(ImGui::GetIO().MousePos.x, ImGui::GetIO().MousePos.y);

            if ((mouse_pos_hb.x < window_pos_hb.x)||(mouse_pos_hb.x > (window_pos_hb.x + window_size_hb.x))||
            (mouse_pos_hb.y < window_pos_hb.y)||(mouse_pos_hb.y > (window_pos_hb.y + window_size_hb.y)))
            {
                b_inside_window_hb = false;
            }
            else
                b_inside_window_hb = true;

            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, heartbeat.heartBeat(up_duration_hb, down_duration_hb, min, max, b_inside_window_hb));

            ImGui::Text("Hello from another window!");

            if (ImGui::Button("Close Me"))
                show_another_window = false;

            ImGui::PopStyleVar();
            ImGui::End();
        }

        // Rendering
        ImGui::Render();
        glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // Update and Render additional Platform Windows
        // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
        //  For this specific demo app we could also call SDL_GL_MakeCurrent(window, gl_context) directly)
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            SDL_Window* backup_current_window = SDL_GL_GetCurrentWindow();
            SDL_GLContext backup_current_context = SDL_GL_GetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            SDL_GL_MakeCurrent(backup_current_window, backup_current_context);
        }

        SDL_GL_SwapWindow(window);

        SDL_Delay(15);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    SDL_GL_DeleteContext(gl_context);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
