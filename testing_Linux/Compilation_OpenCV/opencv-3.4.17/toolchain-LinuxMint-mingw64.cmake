# LinuxMint toolchain from Linux to Windows  mingw32-w64
# Eric Bachard // May 22nd 2017 License GPL v2


# FREETYPE configuration

include(FindFreetype)

include_directories(${FREETYPE_INCLUDE_DIRS})


set(CROSS_PREFIX "/usr/local/cross-tools")
set(TOOLSET x86_64-w64-mingw32)
set(CROSSPATH ${CROSS_PREFIX}/${TOOLSET})

set(PKG_CONFIG_LIBDIR ${CROSSPATH}/lib/pkgconfig/)

set(FREETYPE_LIBRARY_RELEASE "${CROSSPATH}/lib/libfreetype.a")

set(Freetype_FOUND YES)
#set(harfbuzz_FOUND YES)
set(HARFBUZZ_FOUND YES)

set(FREETYPE_LIBRARY "${CROSSPATH}/lib/libfreetype.a")
set(FREETYPE_LIBRARIES "${CROSSPATH}/lib/libfreetype.a")
set(FREETYPE_DIR "${CROSSPATH}/include/freetype2/freetype")
set(FREETYPE_INCLUDE_DIR_freetype2 "${CROSSPATH}/include/freetype2/freetype")
set(FREETYPE_INCLUDE_DIR_ft2build "${CROSSPATH}/include/freetype2/")
set(FREETYPE_INCLUDE_DIRS "${CROSSPATH}/include/freetype2/")

set(HARFBUZZ_DIR "${CROSSPATH}/lib/cmake/harfbuzz")
set(HARFBUZZ_INCLUDE_DIRS "${CROSSPATH}/include/harfbuzz")
set(HARFBUZZ_LIBRARIES "${CROSSPATH}/lib/libharfbuzz.a")
set(PNG_LIBRARY "/usr/local/cross-tools/x86_64-w64-mingw32/lib/libpng.a")
set(PNG_PNG_INCLUDE_DIR "/usr/local/cross-tools/x86_64-w64-mingw32/include/libpng16")

include_directories(${HARFBUZZ_INCLUDE_DIRS})


set(CMAKE_SYSTEM_NAME Windows)
set(CMAKE_SYSTEM_PROCESSOR x86_64)
set(CMAKE_CROSSCOMPILING 1)
set(CMAKE_C_COMPILER_WORKS 1)


set(TOOLCHAIN_PREFIX x86_64-w64-mingw32)

# cross compilers to use for C and C++
set(CMAKE_C_COMPILER ${TOOLCHAIN_PREFIX}-gcc-posix)
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_PREFIX}-g++-posix)
set(CMAKE_RC_COMPILER ${TOOLCHAIN_PREFIX}-windres-posix)

# uncomment if needed
set(DBUILD_SHARED_LIBS OFF)

# target environment on the build host system
#   set 1st to dir with the cross compiler's C/C++ headers/libs
set(CMAKE_FIND_ROOT_PATH /usr/${TOOLCHAIN_PREFIX})

# modify default behavior of FIND_XXX() commands to
# search for headers/libs in the target environment and
# search for programs in the build host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
